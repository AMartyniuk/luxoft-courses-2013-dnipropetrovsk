package com.luxoft.dnepr.courses.compiler;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Stack;

public class ExpressionParser {

    private final String EXPRESSION_REGEXP = "[\\s\\d+\\-*/().]*";

    private final String WRONG_DIGIT_FORMAT_MESSAGE = "Wrong digit format: %s";
    private final String BAD_INPUT_STRING_MESSAGE = "Bad input string! Find bad character!";

    private final char ADD_CHAR = '+';
    private final char SUB_CHAR = '-';
    private final char MUL_CHAR = '*';
    private final char DIV_CHAR = '/';
    private final char OPEN_BRACKET_CHAR = '(';
    private final char CLOSING_BRACKET_CHAR = ')';

    private final byte OPENING_BRACKET = 0x20;
    private final byte CLOSING_BRACKET = 0x21;

    private ByteArrayOutputStream outputStream;

    public byte[] parse(String input) {
        if (!input.matches(EXPRESSION_REGEXP)) {
            throw new CompilationException(BAD_INPUT_STRING_MESSAGE);
        }
        outputStream = new ByteArrayOutputStream();
        Stack<Byte> operators = new Stack<Byte>();
        StringBuilder digitBuilder = new StringBuilder();
        input += " ";
        for (int i = 0; i < input.length(); i++) {
            char symbol = input.charAt(i);
            if (Character.isSpaceChar(symbol)) {
                if (digitBuilder.length() > 0) {
                    pushDigitAndClearBuffer(digitBuilder);
                }
            } else if (Character.isDigit(symbol) || symbol == '.') {
                digitBuilder.append(symbol);
            } else {
                if (digitBuilder.length() > 0) {
                    pushDigitAndClearBuffer(digitBuilder);
                }
                byte operator = getByteCode(symbol);
                if (operator == OPENING_BRACKET) {
                    operators.push(operator);
                    continue;
                }
                if (operator == CLOSING_BRACKET) {
                    while (operators.peek() != OPENING_BRACKET) {
                        addOperator(operators.pop());
                    }
                    operators.pop();
                    continue;
                }
                if (!operators.isEmpty()) {
                    byte top = operators.peek();
                    if (top != OPENING_BRACKET && comparePriority(top, operator) >= 0) {
                        addOperator(operators.pop());
                    }
                }
                operators.push(operator);
            }
        }
        while (!operators.isEmpty()) {
            addOperator(operators.pop());
        }
        Compiler.addCommand(outputStream, VirtualMachine.PRINT);
        byte[] result = outputStream.toByteArray();
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            return result;
        }
    }

    private void pushDigitAndClearBuffer(StringBuilder digitBuilder) {
        String strDigit = digitBuilder.toString();
        if (strDigit.charAt(0) == '.' || strDigit.charAt(strDigit.length() - 1) == '.') {
            throw new CompilationException(String.format(WRONG_DIGIT_FORMAT_MESSAGE, strDigit));
        }
        digitBuilder.delete(0, digitBuilder.length());
        try {
            double value = Double.parseDouble(strDigit);
            Compiler.addCommand(outputStream, VirtualMachine.PUSH, value);
        }
        catch (RuntimeException e) {
            throw new CompilationException(String.format(WRONG_DIGIT_FORMAT_MESSAGE, strDigit));
        }
    }

    private byte getByteCode(char symbol) {
        byte operator = 0;
        switch (symbol) {
            case ADD_CHAR : operator = VirtualMachine.ADD; break;
            case SUB_CHAR : operator = VirtualMachine.SUB; break;
            case MUL_CHAR : operator = VirtualMachine.MUL; break;
            case DIV_CHAR : operator = VirtualMachine.DIV; break;
            case OPEN_BRACKET_CHAR : operator = OPENING_BRACKET; break;
            case CLOSING_BRACKET_CHAR : operator = CLOSING_BRACKET; break;
        }
        return operator;
    }

    private void addOperator(byte operator) {
        if (operator == VirtualMachine.SUB || operator == VirtualMachine.DIV) {
            Compiler.addCommand(outputStream, VirtualMachine.SWAP);
        }
        Compiler.addCommand(outputStream, operator);
    }

    private int comparePriority(byte op1, byte op2) {
        return getPriority(op1) - getPriority(op2);
    }

    private int getPriority(byte op) {
        if (op == VirtualMachine.ADD) {
            return 1;
        }
        if (op == VirtualMachine.SUB) {
            return 2;
        }
        if (op == VirtualMachine.MUL) {
            return 3;
        }
        if (op == VirtualMachine.DIV) {
            return 4;
        }
        return Integer.MAX_VALUE;
    }

}
