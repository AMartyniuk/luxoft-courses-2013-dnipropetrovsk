SELECT maker_id, AVG(screen) as avg_size
FROM product, laptop
WHERE product.model = laptop.model
GROUP BY maker_id
ORDER BY avg_size