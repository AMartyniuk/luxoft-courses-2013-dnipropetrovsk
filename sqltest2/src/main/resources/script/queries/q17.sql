SELECT DISTINCT maker_id FROM product
WHERE model IN (
  SELECT model FROM pc 
  WHERE ram = (SELECT MIN(ram) FROM pc)
  AND speed = (
    SELECT MAX(speed) FROM pc
    WHERE ram = (SELECT MIN(ram) FROM pc)
  )
) AND
maker_id IN (SELECT maker_id FROM product WHERE type = 'Printer')