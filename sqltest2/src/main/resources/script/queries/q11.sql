SELECT maker_id, MAX(type) AS type
FROM product
GROUP BY maker_id
HAVING COUNT(model) > 1 AND MAX(type) = MIN(type)