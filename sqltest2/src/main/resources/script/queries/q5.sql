SELECT DISTINCT pc.model, price FROM makers, product, pc
WHERE 
  makers.maker_id = product.maker_id AND 
  product.model = pc.model AND
  makers.maker_name = 'B'
UNION
SELECT DISTINCT laptop.model, price FROM makers, product, laptop
WHERE 
  makers.maker_id = product.maker_id AND 
  product.model = laptop.model AND
  makers.maker_name = 'B'
UNION
SELECT DISTINCT printer.model, price FROM makers, product, printer
WHERE 
  makers.maker_id = product.maker_id AND 
  product.model = printer.model AND
  makers.maker_name = 'B'