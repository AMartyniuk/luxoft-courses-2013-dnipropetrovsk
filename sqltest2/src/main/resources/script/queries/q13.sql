SELECT type, laptop.model, speed FROM laptop, product
WHERE product.model = laptop.model
AND speed < (SELECT MIN(speed) FROM pc)