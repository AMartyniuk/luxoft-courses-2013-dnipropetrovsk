SELECT DISTINCT maker_name FROM makers
INNER JOIN product ON makers.maker_id = product.maker_id
INNER JOIN pc ON product.model = pc.model
WHERE pc.speed >= 450