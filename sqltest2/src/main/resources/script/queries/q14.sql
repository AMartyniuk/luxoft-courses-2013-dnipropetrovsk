SELECT maker_name, price FROM printer, product, makers
WHERE 
  printer.model = product.model AND
  product.maker_id = makers.maker_id AND
  price = (SELECT MIN(price) FROM printer WHERE printer.color = 'y') AND
  printer.color = 'y'