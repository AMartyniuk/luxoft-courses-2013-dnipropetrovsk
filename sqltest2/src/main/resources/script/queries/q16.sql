SELECT maker_id, COUNT(model) as mod_count
FROM product
WHERE type = 'pc'
GROUP BY maker_id
HAVING COUNT(model) >= 3