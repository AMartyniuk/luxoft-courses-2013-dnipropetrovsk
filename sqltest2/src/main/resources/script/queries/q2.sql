SELECT maker_name, speed FROM laptop 
INNER JOIN product ON product.model = laptop.model 
INNER JOIN makers ON makers.maker_id = product.maker_id  
WHERE laptop.hd >= 10
ORDER BY maker_name DESC