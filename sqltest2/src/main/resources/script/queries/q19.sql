SELECT maker_id, AVG(hd) AS avg_hd FROM product, pc
WHERE product.model = pc.model AND
maker_id IN (
  SELECT maker_id FROM product WHERE product.type = 'Printer'
)
GROUP BY maker_id