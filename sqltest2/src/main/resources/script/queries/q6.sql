SELECT DISTINCT maker_name FROM makers
INNER JOIN product ON makers.maker_id = product.maker_id
WHERE product.type = 'PC' AND maker_name NOT IN 
(SELECT maker_name FROM makers
INNER JOIN product ON makers.maker_id = product.maker_id
WHERE product.type = 'Laptop')