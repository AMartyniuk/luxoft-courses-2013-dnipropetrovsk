SELECT DISTINCT maker_name FROM product, makers
WHERE (product.type = "Printer") AND (product.maker_id = makers.maker_id)
ORDER BY maker_name DESC