package com.luxoft.dnepr.courses.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class HitCountServlet extends HttpServlet {

    private static AtomicInteger counter = new AtomicInteger(0);
    private final String CONTENT_TYPE = "application/json; charset=utf-8";
    private final String RESPONSE_BODY_PATTERN = "{\"hitCount\": %s}";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String responseBody = String.format(RESPONSE_BODY_PATTERN, counter.incrementAndGet());
        resp.setContentType(CONTENT_TYPE);
        resp.setContentLength(responseBody.getBytes().length);
        resp.getWriter().write(responseBody);
    }
}
