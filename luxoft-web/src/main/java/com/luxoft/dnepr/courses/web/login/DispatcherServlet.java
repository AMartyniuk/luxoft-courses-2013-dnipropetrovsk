package com.luxoft.dnepr.courses.web.login;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class DispatcherServlet extends HttpServlet {

    public static final String INDEX_HTML_PAGE = "/myFirstWebApp/index.html";
    public static final String ROOT_PAGE = "/myFirstWebApp/";
    public static final String USER_PAGE = "/myFirstWebApp/user";
    public static final String HOME_PAGE_JSP = "/WEB-INF/pages/index.jsp";
    public static final String USER_PAGE_JSP = "/WEB-INF/pages/welcome.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String query = req.getRequestURI();
        if (query.equals(INDEX_HTML_PAGE) || query.equals(ROOT_PAGE)) {
            req.getRequestDispatcher(HOME_PAGE_JSP).forward(req, resp);
        } else if (query.equalsIgnoreCase(USER_PAGE)) {
            processUserPage(req, resp);
        }
    }

    private void processUserPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session == null || session.getAttribute("login") == null) {
            resp.sendRedirect(INDEX_HTML_PAGE);
        } else {
            req.getRequestDispatcher(USER_PAGE_JSP).forward(req, resp);
        }
    }
}
