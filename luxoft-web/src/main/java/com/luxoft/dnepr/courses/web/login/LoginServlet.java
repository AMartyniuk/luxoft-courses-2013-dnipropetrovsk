package com.luxoft.dnepr.courses.web.login;

import com.luxoft.dnepr.courses.web.login.domain.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        resp.sendRedirect(DispatcherServlet.INDEX_HTML_PAGE);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User user = new User(login, password);
        if (ContextLoadListener.getUsers().contains(user)) {
            HttpSession session = req.getSession();
            session.setAttribute("login", user.getLogin());
            System.out.println(DispatcherServlet.USER_PAGE);
            resp.sendRedirect(DispatcherServlet.USER_PAGE);
        } else {
            req.setAttribute("wrong_login", true);
            req.getRequestDispatcher(DispatcherServlet.HOME_PAGE_JSP).forward(req, resp);
        }
    }

}
