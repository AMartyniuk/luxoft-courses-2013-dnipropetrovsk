package com.luxoft.dnepr.courses.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class DummyServlet extends HttpServlet {

    private final String NAME_PARAMETER = "name";
    private final String AGE_PARAMETER = "age";
    private final String CONTENT_TYPE = "application/json; charset=utf-8";

    private final String POST_ALREADY_EXISTS = "{\"error\": \"Name ${name} already exists\"}";
    private final String PUT_NOT_EXISTS = "{\"error\": \"Name ${name} does not exist\"}";
    private final String ILLEGAL_PARAMETERS = "{\"error\": \"Illegal parameters\"}";

    private static ConcurrentMap<String, String> map = new ConcurrentHashMap<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter(NAME_PARAMETER);
        String age = req.getParameter(AGE_PARAMETER);
        String responseBody = null;
        if (name != null && age != null) {
            if (!map.containsKey(name)) {
                map.put(name, age);
                resp.setStatus(HttpServletResponse.SC_CREATED);
            } else {
                resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                responseBody = POST_ALREADY_EXISTS.replace("${name}", name);
            }
        }
        if (name == null || age == null) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            responseBody = ILLEGAL_PARAMETERS;
        }
        if (responseBody != null) {
            resp.setContentType(CONTENT_TYPE);
            resp.setContentLength(responseBody.getBytes().length);
            resp.getWriter().write(responseBody);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter(NAME_PARAMETER);
        String age = req.getParameter(AGE_PARAMETER);
        String responseBody = null;
        if (name != null && age != null) {
            if (map.containsKey(name)) {
                map.put(name, age);
                resp.setStatus(HttpServletResponse.SC_ACCEPTED);
            } else {
                resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                responseBody = PUT_NOT_EXISTS.replace("${name}", name);
            }
        }
        if (name == null || age == null) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            responseBody = ILLEGAL_PARAMETERS;
        }
        if (responseBody != null) {
            resp.setContentType(CONTENT_TYPE);
            resp.setContentLength(responseBody.getBytes().length);
            resp.getWriter().write(responseBody);
        }
    }

}
