<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Login page</title>
    <link rel="stylesheet" type="text/css" href="style/design.css"/>
</head>
<body>
<div id="wrapper">
    <c:if test="${requestScope.wrong_login}">
        <p id="error">Wrong login or password!</p>
        <br/>
    </c:if>
    <form method="post" action="login">
        <p>Login:</p>
        <input type="text" name="login" placeholder="Login..."/>
        <div class="clear"></div>
        <p>Password:</p>
        <input type="password" name="password" placeholder="Password..."/>
        <div class="clear"></div>
        <input type="submit" value="Sign In"/>
    </form>
</div>
</body>
</html>