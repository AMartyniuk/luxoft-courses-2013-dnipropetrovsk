<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome page</title>
</head>
<body>
    <a href="logout">Logout</a>
    <h1>Welcome, <%= session.getAttribute("login") %>!</h1>
</body>
</html>