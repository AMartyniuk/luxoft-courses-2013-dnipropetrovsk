<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.luxoft.dnepr.courses.web.domain.SessionDataStatistics" %>
<%@ page session="false" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>SessionData Page</title>
    <link rel="stylesheet" type="text/css" href="../style/table_design.css"/>
</head>
<body>
<table>
    <caption>Session Data Table</caption>
    <thead>
    <tr>
        <th>Parameter</th>
        <th>Value</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Active Sessions</td>
        <td><%= SessionDataStatistics.sessionData.get(SessionDataStatistics.SESSION_KEY) %>
        </td>
    </tr>
    <tr>
        <td>Active Sessions (ROLE user)</td>
        <td><%= SessionDataStatistics.sessionData.get(SessionDataStatistics.SESSION_USER_KEY) %>
        </td>
    </tr>
    <tr>
        <td>Active Sessions (ROLE admin)</td>
        <td><%= SessionDataStatistics.sessionData.get(SessionDataStatistics.SESSION_ADMIN_KEY) %>
        </td>
    </tr>
    <tr>
        <td>Total Count of HttpRequests</td>
        <td><%= SessionDataStatistics.sessionData.get(SessionDataStatistics.REQUEST_KEY) %>
        </td>
    </tr>
    <tr>
        <td>Total Count of POST HttpRequests</td>
        <td><%= SessionDataStatistics.sessionData.get(SessionDataStatistics.REQUEST_POST_KEY) %>
        </td>
    </tr>
    <tr>
        <td>Total Count of GET HttpRequests</td>
        <td><%= SessionDataStatistics.sessionData.get(SessionDataStatistics.REQUEST_GET_KEY) %>
        </td>
    </tr>
    <tr>
        <td>Total Count of Other HttpRequests</td>
        <td><%= SessionDataStatistics.sessionData.get(SessionDataStatistics.REQUEST_OTHER_KEY) %>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
