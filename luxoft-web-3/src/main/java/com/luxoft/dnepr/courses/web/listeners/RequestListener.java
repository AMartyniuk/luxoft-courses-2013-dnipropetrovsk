package com.luxoft.dnepr.courses.web.listeners;

import com.luxoft.dnepr.courses.web.domain.SessionDataStatistics;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

public class RequestListener implements ServletRequestListener {

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        HttpServletRequest request = (HttpServletRequest) servletRequestEvent.getServletRequest();
        SessionDataStatistics.addRequest(request);
    }

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        //Nothing to do
    }
}
