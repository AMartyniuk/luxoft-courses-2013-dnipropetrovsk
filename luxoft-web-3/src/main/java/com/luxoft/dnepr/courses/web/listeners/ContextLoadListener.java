package com.luxoft.dnepr.courses.web.listeners;

import com.luxoft.dnepr.courses.web.domain.User;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ContextLoadListener implements ServletContextListener {

    private static final String USERS_PARAMETER = "users";
    private static final String BASE_DIR = "META-INF/";

    private static final String USER_TAG = "user";

    private static final String NAME_ATTRIBUTE = "name";
    private static final String PASSWORD_ATTRIBUTE = "password";
    private static final String ROLE_ATTRIBUTE = "role";

    private static List<User> userList = new ArrayList<>();

    public static List<User> getUsers() {
        return userList;
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String usersFile = servletContextEvent.getServletContext().getInitParameter(USERS_PARAMETER);
        InputStream is = servletContextEvent.getServletContext().getResourceAsStream(BASE_DIR + usersFile);
        parseXML(is);
    }

    private void parseXML(InputStream is) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);
            NodeList nodeList = doc.getElementsByTagName(USER_TAG);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element userElement = (Element) nodeList.item(i);
                String login = userElement.getAttribute(NAME_ATTRIBUTE);
                String password = userElement.getAttribute(PASSWORD_ATTRIBUTE);
                String role = userElement.getAttribute(ROLE_ATTRIBUTE);
                userList.add(new User(login, password, role));
            }
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        //Nothing to do
    }
}
