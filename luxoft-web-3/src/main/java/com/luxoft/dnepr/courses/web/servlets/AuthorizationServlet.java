package com.luxoft.dnepr.courses.web.servlets;

import com.luxoft.dnepr.courses.web.domain.SessionDataStatistics;
import com.luxoft.dnepr.courses.web.domain.User;
import com.luxoft.dnepr.courses.web.listeners.ContextLoadListener;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthorizationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.invalidate();
        resp.sendRedirect("index.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User user = new User(login, password, null);
        if (ContextLoadListener.getUsers().contains(user)) {
            int index = ContextLoadListener.getUsers().indexOf(user);
            user = ContextLoadListener.getUsers().get(index);
            HttpSession session = req.getSession();
            session.setAttribute("login", user.getLogin());
            session.setAttribute("role", user.getRole());
            SessionDataStatistics.updateRoleSession(session);
            resp.sendRedirect("user");
        } else {
            req.setAttribute("wrong_login", true);
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        }
    }
}
