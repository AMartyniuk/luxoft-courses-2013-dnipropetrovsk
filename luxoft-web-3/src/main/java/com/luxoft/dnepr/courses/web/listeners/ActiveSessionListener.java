package com.luxoft.dnepr.courses.web.listeners;

import com.luxoft.dnepr.courses.web.domain.SessionDataStatistics;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class ActiveSessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        SessionDataStatistics.createSession();
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        SessionDataStatistics.destroySession(httpSessionEvent.getSession());
    }
}
