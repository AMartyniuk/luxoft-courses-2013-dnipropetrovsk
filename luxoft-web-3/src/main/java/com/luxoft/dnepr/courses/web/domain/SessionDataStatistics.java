package com.luxoft.dnepr.courses.web.domain;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public final class SessionDataStatistics {

    public static final String SESSION_KEY = "session";
    public static final String SESSION_ADMIN_KEY = "session_admin";
    public static final String SESSION_USER_KEY = "session_user";

    public static final String REQUEST_KEY = "request";
    public static final String REQUEST_GET_KEY = "request_get";
    public static final String REQUEST_POST_KEY = "request_post";
    public static final String REQUEST_OTHER_KEY = "request_other";

    public static final String ROLE_ATTRIBUTE = "role";
    public static final String USER_ROLE = "user";
    public static final String ADMIN_ROLE = "admin";

    public static final Map<String, AtomicLong> sessionData = new HashMap<>();

    static {
        sessionData.put(SESSION_KEY, new AtomicLong(0));
        sessionData.put(SESSION_ADMIN_KEY, new AtomicLong(0));
        sessionData.put(SESSION_USER_KEY, new AtomicLong(0));
        sessionData.put(REQUEST_KEY, new AtomicLong(0));
        sessionData.put(REQUEST_POST_KEY, new AtomicLong(0));
        sessionData.put(REQUEST_GET_KEY, new AtomicLong(0));
        sessionData.put(REQUEST_OTHER_KEY, new AtomicLong(0));
    }

    public static void createSession() {
        increaseParameter(SESSION_KEY);
    }

    public static void updateRoleSession(HttpSession session) {
        if (session.getAttribute(ROLE_ATTRIBUTE).equals(USER_ROLE)) {
            increaseParameter(SESSION_USER_KEY);
        } else if (session.getAttribute(ROLE_ATTRIBUTE).equals(ADMIN_ROLE)) {
            increaseParameter(SESSION_ADMIN_KEY);
        }
    }

    public static void destroySession(HttpSession session) {
        decreaseParameter(SESSION_KEY);
        if (session.getAttribute(ROLE_ATTRIBUTE).equals(USER_ROLE)) {
            decreaseParameter(SESSION_USER_KEY);
        } else if (session.getAttribute(ROLE_ATTRIBUTE).equals(ADMIN_ROLE)) {
            decreaseParameter(SESSION_ADMIN_KEY);
        }
    }

    public static void addRequest(HttpServletRequest request) {
        increaseParameter(REQUEST_KEY);
        if (request.getMethod().equalsIgnoreCase("get")) {
            increaseParameter(REQUEST_GET_KEY);
        } else if (request.getMethod().equalsIgnoreCase("post")) {
            increaseParameter(REQUEST_POST_KEY);
        } else {
            increaseParameter(REQUEST_OTHER_KEY);
        }
    }

    private static void increaseParameter(String parameter) {
        AtomicLong counter = sessionData.get(parameter);
        counter.incrementAndGet();
    }

    private static void decreaseParameter(String parameter) {
        AtomicLong counter = sessionData.get(parameter);
        counter.decrementAndGet();
    }

}
