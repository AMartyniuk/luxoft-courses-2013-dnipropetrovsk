package com.luxoft.dnepr.courses.unit3.view;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class CommandLineInterface {

    private final String INVALID_COMMAND_MESSAGE = "Invalid command";
    private final String WIN_MESSAGE = "Congrats! You win!";
    private final String PUSH_MESSAGE = "Push. Everybody has equal amount of points.";
    private final String LOOSE_MESSAGE = "Sorry, today is not your day. You loose.";

    private final String DEALER_TURN = "Dealer turn:";
    private final String TOTAL_FORMAT = "(total %d)";

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: Andrii Martyniuk\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();

        controller.newGame();

        output.println();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     *
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     * Описание команд:
     * Command.HELP - печатает помощь.
     * Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *                если после карты игрок проиграл - напечатать финальное сообщение и выйти
     * Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     *                после того как диллер сыграл напечатать:
     *                Dealer turn:
     *                пустая строка
     *                состояние
     *                пустая строка
     *                финальное сообщение
     * Command.EXIT - выйти из игры
     *
     * Состояние:
     * рука игрока (total вес)
     * рука диллера (total вес)
     *
     * например:
     * 3 J 8 (total 21)
     * A (total 11)
     *
     * Финальное сообщение:
     * В зависимости от состояния печатаем:
     * Congrats! You win!
     * Push. Everybody has equal amount of points.
     * Sorry, today is not your day. You loose.
     *
     * Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */
    private boolean execute(String command, GameController controller) {
        boolean executeResult = true;
        if (command.equals(Command.HELP)) {
            printHelp();
        } else if (command.equals(Command.MORE)) {
            boolean goodTurn = controller.requestMore();
            printState(controller);
            if (!goodTurn) {
                printWinState(controller.getWinState());
                executeResult = false;
            }
        } else if (command.equals(Command.STOP)) {
            controller.requestStop();
            printDealer();
            printWinState(controller.getWinState());
            executeResult = false;
        } else if (command.equals(Command.EXIT)) {
            executeResult = false;
        } else {
            output.println(INVALID_COMMAND_MESSAGE);
        }
        return executeResult;
    }

    private void printWinState(WinState winState) {
        output.println();
        if (winState == WinState.WIN) {
            output.println(WIN_MESSAGE);
        } else if (winState == WinState.PUSH) {
            output.println(PUSH_MESSAGE);
        } else {
            output.println(LOOSE_MESSAGE);
        }
    }

    private void printDealer() {
        output.println(DEALER_TURN);
        output.println();
        printState(GameController.getInstance());
    }

    private void printHelp() {
        output.println("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game");
    }

    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();
        format(myHand);
        List<Card> dealersHand = controller.getDealersHand();
        format(dealersHand);
    }

    private void format(List<Card> hand) {
        for (Card card : hand) {
            output.print(card.getRank().getName() + " ");
        }
        output.println(String.format(TOTAL_FORMAT, Deck.costOf(hand)));
    }
}
