package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

public final class Deck {

    private static final int MIN_DECK_COUNT = 1;
    private static final int MAX_DECK_COUNT = 10;

    private Deck() {
    }

    public static List<Card> createDeck(int size) {
        if (size < MIN_DECK_COUNT) {
            size = MIN_DECK_COUNT;
        }
        if (size > MAX_DECK_COUNT) {
            size = MAX_DECK_COUNT;
        }
        List<Card> deck = new ArrayList<Card>();
        for (int i = 0; i < size; i++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    deck.add(new Card(rank, suit));
                }
            }
        }
        return deck;
    }

    public static int costOf(List<Card> hand) {
        int cost = 0;
        for (Card card : hand) {
            cost += card.getCost();
        }
        return cost;
    }
}
