package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public final class GameController {

    private final int FIRST_CARD = 0;
    private final int BLACKJACK = 21;
    private final int DEALER_MIN_COST = 17;
    private final int DECK_COUNT = 2;

    private static GameController controller;

    private List<Card> deck;

    private List<Card> userHand;
    private List<Card> dealerHand;

    private GameController() {
        userHand = new ArrayList<Card>();
        dealerHand = new ArrayList<Card>();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {

        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Create new game.
     * - shuffle deck (uses shuffler.shuffle(list))
     * - get two cards to user
     * - get one card to dealer.
     *
     * @param shuffler object that shuffle deck
     */
    void newGame(Shuffler shuffler) {
        deck = Deck.createDeck(DECK_COUNT);
        userHand.clear();
        dealerHand.clear();
        shuffler.shuffle(deck);
        userHand.add(deck.remove(FIRST_CARD));
        userHand.add(deck.remove(FIRST_CARD));
        dealerHand.add(deck.remove(FIRST_CARD));
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        if (Deck.costOf(userHand) <= BLACKJACK && !userHand.isEmpty()) {
            userHand.add(deck.remove(FIRST_CARD));
        }
        return Deck.costOf(userHand) <= BLACKJACK;
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        while (Deck.costOf(dealerHand) < DEALER_MIN_COST) {
            dealerHand.add(deck.remove(FIRST_CARD));
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        int userCost = Deck.costOf(userHand);
        int dealerCost = Deck.costOf(dealerHand);
        if (userCost > BLACKJACK) {
            return WinState.LOOSE;
        } else if (userCost < dealerCost && dealerCost <= BLACKJACK) {
            return WinState.LOOSE;
        } else if (userCost == dealerCost) {
            return WinState.PUSH;
        } else if (userCost > dealerCost && userCost <= BLACKJACK) {
            return WinState.WIN;
        }
        return WinState.WIN;
    }

    /**
     * Return user's hand
     */
    public List<Card> getMyHand() {
        return Collections.unmodifiableList(userHand);
    }

    /**
     * Return dealer's hand
     */
    public List<Card> getDealersHand() {
        return Collections.unmodifiableList(dealerHand);
    }
}
