package com.luxoft.dnepr.courses.unit3.model;

public enum Rank {

    RANK_ACE("ACE", 11),
    RANK_2("2", 2),
    RANK_3("3", 3),
    RANK_4("4", 4),
    RANK_5("5", 5),
    RANK_6("6", 6),
    RANK_7("7", 7),
    RANK_8("8", 8),
    RANK_9("9", 9),
    RANK_10("10", 10),
    RANK_JACK("JACK", 10),
    RANK_QUEEN("QUEEN", 10),
    RANK_KING("KING", 10);

    private String name;
    private int cost;

    private Rank(String name, int cost) {
        this.name = name;
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

}
