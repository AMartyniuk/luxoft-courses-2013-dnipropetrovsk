//GameController constructor function
function GameController() {
    this.endGame = false;
}

//GameController static constants
GameController.PLAYER_HAND_ID = "#player";
GameController.DEALER_HAND_ID = "#dealer";
GameController.PLAYER_SCORE_ID = "#player-score";
GameController.DEALER_SCORE_ID = "#dealer-score";
GameController.MESSAGE_ID = "#message";
GameController.WON_MESSAGE = "You won, congratulations! Click 'New Game' to play again!";
GameController.PUSH_MESSAGE = "Push state! Click 'New Game' to play again!";
GameController.LOSE_MESSAGE = "You lose, sorry. Click 'New Game' to play again!";

//functions of GameController
GameController.prototype.updateUI = function(gameState) {
    this.clearUI();
    this.showHand(gameState.myhand, $(GameController.PLAYER_HAND_ID), $(GameController.PLAYER_SCORE_ID));
    this.showHand(gameState.dealer, $(GameController.DEALER_HAND_ID), $(GameController.DEALER_SCORE_ID));
    //response after Stop command
    if (gameState.dealer !== undefined) {
        this.endGame = true;
        //player won
        if (gameState.result === "true") {
            $(GameController.MESSAGE_ID).text(GameController.WON_MESSAGE);
            $(GameController.MESSAGE_ID).attr("class", "won");
        } else {
            //push state
            if (this.getScore(gameState.myhand) === this.getScore(gameState.dealer)) {
                $(GameController.MESSAGE_ID).text(GameController.PUSH_MESSAGE);
                $(GameController.MESSAGE_ID).attr("class", "push");
            //lose state
            } else {
                $(GameController.MESSAGE_ID).text(GameController.LOSE_MESSAGE);
                $(GameController.MESSAGE_ID).attr("class", "lose");
            }
        }
    } else {
        this.endGame = (gameState.result === "false");
        //player has more than 21 score and lose
        if (this.endGame) {
            $(GameController.MESSAGE_ID).text(GameController.LOSE_MESSAGE);
            $(GameController.MESSAGE_ID).attr("class", "lose");
        }
    }
}

GameController.prototype.getScore = function(hand) {
    var score = 0;
    for (var i = 0; i < hand.length; i++) {
        score += hand[i].cost;
    }
    return score;
}

GameController.prototype.clearUI = function() {
    $(GameController.MESSAGE_ID).text("");
    $(GameController.PLAYER_HAND_ID).empty();
    $(GameController.DEALER_HAND_ID).empty();
    $(GameController.PLAYER_SCORE_ID).text(0);
    $(GameController.DEALER_SCORE_ID).text(0);
}

GameController.prototype.showHand = function(hand, parentElement, scoreElement) {
    if (hand !== undefined) {
        for (var i = 0; i < hand.length; i++) {
            var card = hand[i];
            var imgFile = card.rank + "_of_" + card.suit.toLowerCase() + ".png";
            var src = "img/cards/" + imgFile;
            var img = $("<img>").attr("src", src);
            parentElement.append(img);
        }
        scoreElement.text(this.getScore(hand));
    }
}

var game = new GameController();

//set button click handlers
$(function() {
    var newGameId = "#new-game";
    var requestMoreId = "#request-more";
    var requestStopId = "#request-stop";

    $(newGameId).click(createHandler("/service?method=newGame"));
    $(requestMoreId).click(createHandler("/service?method=requestMore"));
    $(requestStopId).click(createHandler("/service?method=requestStop"));

    function createHandler(url) {
        return function(event) {
            if (game.endGame && !(url.indexOf("newGame") > 0)) {
                event.preventDefault();
                return;
            }
            var data = {url: url, data : {'ie_fix_cache_request' : (new Date()).getTime()}};
            $.ajax(data).done(function(json) {
                game.updateUI($.parseJSON(json));
            });
            event.preventDefault();
       }
    }
});
