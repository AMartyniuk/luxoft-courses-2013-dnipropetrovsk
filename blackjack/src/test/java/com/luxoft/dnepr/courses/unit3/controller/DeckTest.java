package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

import static org.junit.Assert.*;

public class DeckTest {

    @Test
    public void testCreate() {
        assertEquals(52, Deck.createDeck(1).size());
        assertEquals(104, Deck.createDeck(2).size());
        assertEquals(208, Deck.createDeck(4).size());
        assertEquals(52 * 10, Deck.createDeck(10).size());

        assertEquals(52 * 10, Deck.createDeck(11).size());
        assertEquals(52, Deck.createDeck(-1).size());

        List<Card> deck = Deck.createDeck(2);
        int i = 0;

        for (int deckN = 0; deckN < 2; deckN++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    assertEquals(suit, deck.get(i).getSuit());
                    assertEquals(rank, deck.get(i).getRank());
                    assertEquals(rank.getCost(), deck.get(i).getCost());
                    i++;
                }
            }
        }
    }

    @Test
    public void testCostOf() {
        List<Card> cards = new ArrayList<Card>(Arrays.asList(
                //He may play the jack of diamonds
                new Card(Rank.RANK_JACK, Suit.DIAMONDS),
                //He may lay the queen of spades
                new Card(Rank.RANK_QUEEN, Suit.SPADES),
                //He may conceal a king in his hand
                //While the memory of it fades
                new Card(Rank.RANK_KING, Suit.CLUBS)
        ));
        assertEquals(30, Deck.costOf(cards));
        cards.add(new Card(Rank.RANK_6, Suit.DIAMONDS));
        assertEquals(36, Deck.costOf(cards));
    }
}
