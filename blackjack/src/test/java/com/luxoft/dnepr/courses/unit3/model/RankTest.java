package com.luxoft.dnepr.courses.unit3.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class RankTest {

    @Test
    public void rankCostTest() {
        assertEquals(2, Rank.RANK_2.getCost());
        assertEquals(3, Rank.RANK_3.getCost());
        assertEquals(4, Rank.RANK_4.getCost());
        assertEquals(5, Rank.RANK_5.getCost());
        assertEquals(6, Rank.RANK_6.getCost());
        assertEquals(7, Rank.RANK_7.getCost());
        assertEquals(8, Rank.RANK_8.getCost());
        assertEquals(9, Rank.RANK_9.getCost());
        assertEquals(10, Rank.RANK_10.getCost());
        assertEquals(10, Rank.RANK_JACK.getCost());
        assertEquals(10, Rank.RANK_QUEEN.getCost());
        assertEquals(10, Rank.RANK_KING.getCost());
        assertEquals(11, Rank.RANK_ACE.getCost());
    }

}
