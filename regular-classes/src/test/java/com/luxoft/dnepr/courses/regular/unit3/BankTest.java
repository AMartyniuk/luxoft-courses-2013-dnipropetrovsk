package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class BankTest {

    private final String JAVA_VERSION_KEY = "java.version";
    private final String ACTUAL_JAVA_VERSION;
    private final String NOT_ENOUGH_MONEY_MESSAGE = "User 'John' has insufficient funds (3000.00 < 3100.00)";
    private final String USER_BLOCKED_MESSAGE = "User 'John' wallet is blocked";
    private final String LIMIT_EXCEEDED_MESSAGE = "User 'Harry' wallet limit exceeded (300.00 + 400.00 > 500.00)";

    {
        ACTUAL_JAVA_VERSION = System.getProperty(JAVA_VERSION_KEY);
    }

    private Map<Long, UserInterface> users;

    @Before
    public void initUsers() {
        users = new HashMap<>();
        UserInterface user1 = new User(1L, "Ivan");
        UserInterface user2 = new User(2L, "Andrii");
        UserInterface user3 = new User(3L, "John");
        UserInterface user4 = new User(4L, "Harry");
        WalletInterface wallet1 = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(2000));
        WalletInterface wallet2 = new Wallet(2L, BigDecimal.valueOf(1000), BigDecimal.valueOf(1500));
        WalletInterface wallet3 = new Wallet(3L, BigDecimal.valueOf(3000), BigDecimal.valueOf(4000));
        WalletInterface wallet4 = new Wallet(4L, BigDecimal.valueOf(300), BigDecimal.valueOf(500));
        user1.setWallet(wallet1);
        user2.setWallet(wallet2);
        user3.setWallet(wallet3);
        user4.setWallet(wallet4);
        users.put(user1.getId(), user1);
        users.put(user2.getId(), user2);
        users.put(user3.getId(), user3);
        users.put(user4.getId(), user4);
    }

    @Test(expected = IllegalJavaVersionError.class)
    public void createBankOnWrongJVMTest() {
        String wrongJVMVersion = "1.0";
        new Bank(wrongJVMVersion);
    }

    @Test
    public void makeMoneyTransactionNormalTest() {
        BankInterface bank = new Bank(ACTUAL_JAVA_VERSION);
        bank.setUsers(users);
        try {
            bank.makeMoneyTransaction(2L, 1L, BigDecimal.valueOf(900));
        } catch (NoUserFoundException | TransactionException e) {
            Assert.fail();
        }
        Assert.assertEquals(users.get(1L).getWallet().getAmount(), BigDecimal.valueOf(1000));
        Assert.assertEquals(users.get(2L).getWallet().getAmount(), BigDecimal.valueOf(100));
    }

    @Test
    public void makeMoneyTransactionNotFoundUserTest() {
        BankInterface bank = new Bank(ACTUAL_JAVA_VERSION);
        bank.setUsers(users);
        try {
            bank.makeMoneyTransaction(22L, 1L, BigDecimal.valueOf(900));
        } catch (TransactionException e) {
            Assert.fail();
        } catch (NoUserFoundException e) {
            Assert.assertEquals(e.getUserId(), new Long(22L));
        }
    }

    @Test
    public void makeMoneyTransactionErrorTest() {
        BankInterface bank = new Bank(ACTUAL_JAVA_VERSION);
        bank.setUsers(users);
        try {
            bank.makeMoneyTransaction(3L, 1L, BigDecimal.valueOf(3100));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), NOT_ENOUGH_MONEY_MESSAGE);
        } catch (NoUserFoundException e) {
            Assert.fail();
        }
    }

    @Test
    public void makeMoneyTransactionUserWalletBlockedTest() {
        BankInterface bank = new Bank(ACTUAL_JAVA_VERSION);
        bank.setUsers(users);
        try {
            users.get(3L).getWallet().setStatus(WalletStatus.BLOCKED);
            bank.makeMoneyTransaction(3L, 1L, BigDecimal.valueOf(3100));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), USER_BLOCKED_MESSAGE);
        } catch (NoUserFoundException e) {
            Assert.fail();
        }
        users.get(3L).getWallet().setStatus(WalletStatus.ACTIVE);
    }

    @Test
    public void makeMoneyTransactionLimitExceededTest() {
        BankInterface bank = new Bank(ACTUAL_JAVA_VERSION);
        bank.setUsers(users);
        try {
            bank.makeMoneyTransaction(3L, 4L, BigDecimal.valueOf(400));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), LIMIT_EXCEEDED_MESSAGE);
        } catch (NoUserFoundException e) {
            Assert.fail();
        }
    }

}
