package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();
        assertNotSame(book.getPublicationDate(), cloned.getPublicationDate());

    }

    @Test
    public void testEquals() throws Exception {
        Book firstBook = productFactory.createBook("code", "Effective Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book secondBook = productFactory.createBook("code", "Effective Java", 220, new GregorianCalendar(2006, 0, 1).getTime());
        assertEquals(secondBook, firstBook);
    }
}
