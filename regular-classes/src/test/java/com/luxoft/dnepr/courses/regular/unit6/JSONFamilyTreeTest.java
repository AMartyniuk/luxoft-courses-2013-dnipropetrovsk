package com.luxoft.dnepr.courses.regular.unit6;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import junit.framework.Assert;
import org.junit.Test;

import java.io.*;

public class JSONFamilyTreeTest {

    @Test
    public void saveToFileTest() {
        PersonImpl person = new PersonImpl("Paraska", Gender.FEMALE, "ukrainian", 19, null, null);
        PersonImpl father = new PersonImpl("Vasiliy", Gender.MALE, "ukrainian", 40, null, null);
        PersonImpl mother = new PersonImpl("Ludmila", Gender.FEMALE, "ukrainian", 39, null, null);
        person.setFather(father);
        person.setMother(mother);

        FamilyTree familyTree = FamilyTreeImpl.create(person);

        IOUtils.save("file.txt", familyTree);
        FamilyTree fromJSON = IOUtils.load("file.txt");

        new File("file.txt").delete();

        Assert.assertEquals(familyTree.getRoot(), fromJSON.getRoot());
    }

    @Test
    public void saveToStreamTest() throws IOException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            PersonImpl person = new PersonImpl("Ivan", Gender.MALE, "ukrainian", 18, null, null);
            PersonImpl mother = new PersonImpl("Marina", Gender.FEMALE, "ukrainian", 38, null, null);
            PersonImpl father = new PersonImpl("Igor", Gender.MALE, "ukrainian", 40, null, null);
            PersonImpl grandFather = new PersonImpl("Vitalii Ivanovich", Gender.MALE, "ukrainian", 67, null, null);
            person.setMother(mother);
            person.setFather(father);
            father.setFather(grandFather);

            FamilyTree familyTree = FamilyTreeImpl.create(person);

            IOUtils.save(baos, familyTree);

            try (ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray())) {
                FamilyTree familyTreeFromJSON = IOUtils.load(bais);
                Assert.assertEquals(familyTree.getRoot(), familyTreeFromJSON.getRoot());
            }

        }
    }

}
