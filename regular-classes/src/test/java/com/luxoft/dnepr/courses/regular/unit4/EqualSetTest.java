package com.luxoft.dnepr.courses.regular.unit4;


import junit.framework.Assert;
import org.junit.Test;

import java.util.*;

public class EqualSetTest {

    @Test
    public void addTest() {
        Set<Integer> set = new EqualSet<>();
        set.add(1);
        set.add(1);
        set.add(5);
        set.add(5);
        Assert.assertEquals(set.size(), 2);
    }

    @Test
    public void emptySetTest() {
        Set<Integer> set = new EqualSet<>();
        Assert.assertTrue(set.isEmpty());
    }

    @Test
    public void collectionConstructorSetTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5);
        Set<Integer> set = new EqualSet<>(list);
        Assert.assertEquals(set.size(), 5);
    }

    @Test
    public void setIteratorTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5);
        Set<Integer> set = new EqualSet<>(list);
        List<Integer> iterList = new ArrayList<>();
        for (Integer aSet : set) {
            iterList.add(aSet);
        }
        Assert.assertEquals(iterList.size(), 5);
    }

    @Test
    public void setIteratorRemoveTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5);
        Set<Integer> set = new EqualSet<>(list);
        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
        Assert.assertTrue(set.isEmpty());
    }

    @Test(expected = IllegalStateException.class)
    public void setIteratorWrongRemoveTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5);
        Set<Integer> set = new EqualSet<>(list);
        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            it.next();
            it.remove();
            it.remove();
        }
        Assert.fail();
    }

    @Test(expected = NoSuchElementException.class)
    public void iteratorNextErrorTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5);
        Set<Integer> set = new EqualSet<>(list);
        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            it.next();
        }
        it.next();
        Assert.fail();
    }

    @Test
    public void nullItemTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5);
        Set<Integer> set = new EqualSet<>(list);
        set.add(null);
        set.add(null);
        set.add(6);
        set.add(null);
        Assert.assertEquals(set.size(), 7);
    }

    @Test
    public void emptySetHasNextIterTest() {
        Set<Integer> set = new EqualSet<>();
        Assert.assertTrue(!set.iterator().hasNext());
    }

    @Test
    public void equalMethodSetTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5);
        Set<Integer> set = new EqualSet<>(list);
        Set<Integer> otherSet = new EqualSet<>(list);
        otherSet.add(5);
        otherSet.add(3);
        otherSet.add(2);
        otherSet.add(1);
        otherSet.add(4);
        Assert.assertTrue(set.equals(otherSet));
    }

    @Test
    public void hashCodeMethodTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5, null);
        Set<Integer> set = new EqualSet<>(list);
        Set<Integer> otherSet = new EqualSet<>(list);
        otherSet.add(5);
        otherSet.add(3);
        otherSet.add(2);
        otherSet.add(1);
        otherSet.add(null);
        otherSet.add(4);
        Assert.assertTrue(set.hashCode() == otherSet.hashCode());
    }

    @Test
    public void clearMethodTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5, null);
        Set<Integer> set = new EqualSet<>(list);
        set.clear();
        Assert.assertTrue(set.isEmpty());
    }

    @Test
    public void removeMethodTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5, null);
        Set<Integer> set = new EqualSet<>(list);
        set.remove(1);
        set.remove(2);
        set.remove(null);
        Assert.assertEquals(set.size(), 3);
    }

    @Test
    public void removeAllMethodTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 5, 5, 5, null);
        Set<Integer> set = new EqualSet<>(list);
        set.removeAll(Arrays.asList(1, 3, null, null, 1, 1));
        Assert.assertEquals(set.size(), 3);
    }

    @Test
    public void addMethodTest() {
        Integer x = 1;
        Integer y = 1;
        Set<Integer> set = new EqualSet<>();
        set.add(x);
        set.add(y);
        Integer value = set.iterator().next();
        Assert.assertSame(x, value);
    }

    @Test
    public void hashCodeMissObjectsTest() {
        Set<Person> set = new EqualSet<>();
        Person p1 = new Person("Andrii");
        Person p2 = new Person("Andrii");
        Person p3 = new Person("Andrii");
        set.addAll(Arrays.asList(p1, p2, p3));
        Assert.assertEquals(set.size(), 1);
    }

    private class Person {

        private Random random = new Random();
        private String name;

        public Person() {}

        public Person(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null || !(obj instanceof Person)) {
                return false;
            }
            Person other = (Person) obj;
            return Objects.equals(name, other.name);
        }

        //If set uses hashCode test fails
        @Override
        public int hashCode() {
            Assert.fail();
            return random.nextInt();
        }
    }


}
