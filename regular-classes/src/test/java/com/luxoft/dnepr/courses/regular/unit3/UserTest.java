package com.luxoft.dnepr.courses.regular.unit3;


import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class UserTest {

    @Test
    public void equalTest() {
        User user = new User(1L, "Ivan");
        WalletInterface wallet = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(5000));
        user.setWallet(wallet);

        User userAnother = new User(1L, "Ivan");
        WalletInterface walletAnother = new Wallet(2L, BigDecimal.valueOf(50), BigDecimal.valueOf(1000));
        userAnother.setWallet(walletAnother);

        Assert.assertEquals(user, userAnother);
    }

}
