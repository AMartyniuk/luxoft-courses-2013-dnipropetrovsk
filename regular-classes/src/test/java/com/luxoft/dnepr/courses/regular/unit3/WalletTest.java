package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class WalletTest {

    @Test
    public void checkWithdrawalNormalTest() {
        WalletInterface wallet = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(500));
        try {
            wallet.checkWithdrawal(BigDecimal.valueOf(80));
        } catch (WalletIsBlockedException | InsufficientWalletAmountException e) {
            Assert.fail();
        }
    }

    @Test
    public void checkWithdrawalNotEnoughMoneyTest() {
        WalletInterface wallet = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(500));
        try {
            wallet.checkWithdrawal(BigDecimal.valueOf(120));
        } catch (WalletIsBlockedException e) {
            Assert.fail();
        } catch (InsufficientWalletAmountException e) {
            Assert.assertEquals(e.getAmountInWallet(), BigDecimal.valueOf(100));
            Assert.assertEquals(e.getAmountToWithdraw(), BigDecimal.valueOf(120));
            Assert.assertEquals(e.getWalletId(), wallet.getId());
        }
    }

    @Test
    public void checkWithdrawalBlockedTest() {
        WalletInterface wallet = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(500));
        wallet.setStatus(WalletStatus.BLOCKED);
        try {
            wallet.checkWithdrawal(BigDecimal.valueOf(120));
        } catch (WalletIsBlockedException e) {
            Assert.assertEquals(e.getWalletId(), wallet.getId());
        } catch (InsufficientWalletAmountException e) {
            Assert.fail();
        }
    }

    @Test
    public void withdrawTest() {
        WalletInterface wallet = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(500));
        wallet.withdraw(BigDecimal.valueOf(80));
        Assert.assertEquals(wallet.getAmount(), BigDecimal.valueOf(20));
    }

    @Test
    public void checkTransferNormalTest() {
        WalletInterface wallet = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(500));
        try {
            wallet.checkTransfer(BigDecimal.valueOf(250));
        } catch (WalletIsBlockedException | LimitExceededException e) {
            Assert.fail();
        }
    }

    @Test
    public void checkTransferLimitExceededTest() {
        WalletInterface wallet = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(500));
        try {
            wallet.checkTransfer(BigDecimal.valueOf(500));
        } catch (WalletIsBlockedException e) {
            Assert.fail();
        } catch (LimitExceededException e) {
            Assert.assertEquals(e.getWalletId(), wallet.getId());
            Assert.assertEquals(e.getAmountInWallet(), wallet.getAmount());
            Assert.assertEquals(e.getAmountToTransfer(), BigDecimal.valueOf(500));
        }
    }

    @Test
    public void checkTransferBlockedTest() {
        WalletInterface wallet = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(500));
        try {
            wallet.checkTransfer(BigDecimal.valueOf(220));
        } catch (WalletIsBlockedException e) {
            Assert.assertEquals(e.getWalletId(), wallet.getId());
        } catch (LimitExceededException e) {
            Assert.fail();
        }
    }

    @Test
    public void transferTest() {
        WalletInterface wallet = new Wallet(1L, BigDecimal.valueOf(100), BigDecimal.valueOf(500));
        wallet.transfer(BigDecimal.valueOf(400));
        Assert.assertEquals(wallet.getAmount(), BigDecimal.valueOf(500));
    }

}
