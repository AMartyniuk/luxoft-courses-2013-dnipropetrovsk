package com.luxoft.dnepr.courses.regular.unit17;

import com.luxoft.dnepr.courses.regular.unit17.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit17.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit17.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit17.model.Employee;
import com.luxoft.dnepr.courses.regular.unit17.model.Redis;
import com.luxoft.dnepr.courses.regular.unit17.storage.EntityStorage;
import junit.framework.Assert;
import org.junit.Test;

public class DaoImplTest {

    private EntityStorage storage = new EntityStorage();
    private IDao<Employee> employeeDao = new EmployeeDaoImpl(storage);
    private IDao<Redis> redisDao = new RedisDaoImpl(storage);

    @Test
    public void employeeDaoTest() {
        //Create 2 employees
        Employee employee1 = new Employee(350);
        Employee employee2 = new Employee(700);
        //Save employees in database
        employee1 = employeeDao.save(employee1);
        employee2 = employeeDao.save(employee2);
        //Get it from database
        Employee get1 = employeeDao.get(employee1.getId());
        Employee get2 = employeeDao.get(employee2.getId());
        //Assert employees
        Assert.assertEquals(employee1, get1);
        Assert.assertEquals(employee2, get2);
        //Change salary of employee1
        employee1.setSalary(1000);
        employeeDao.update(employee1);
        //Check update state
        Employee updated = employeeDao.get(employee1.getId());
        Assert.assertEquals(1000, updated.getSalary());
        //Delete changed employee
        boolean success = employeeDao.delete(updated);
        Assert.assertTrue(success);
        Employee mustBeDeleted = employeeDao.get(updated.getId());
        Assert.assertNull(mustBeDeleted);
    }

    @Test
    public void redisDaoTest() {
        //Create 2 redis
        Redis redis1 = new Redis(35);
        Redis redis2 = new Redis(70);
        //Save redis in database
        redis1 = redisDao.save(redis1);
        redis2 = redisDao.save(redis2);
        //Get it from database
        Redis get1 = redisDao.get(redis1.getId());
        Redis get2 = redisDao.get(redis2.getId());
        //Assert redis
        Assert.assertEquals(redis1, get1);
        Assert.assertEquals(redis2, get2);
        //Change salary of employee1
        redis1.setWeight(100);
        redisDao.update(redis1);
        //Check update state
        Redis updated = redisDao.get(redis1.getId());
        Assert.assertEquals(100, updated.getWeight());
    }

}
