package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class LuxoftUtilsTest {

    private static double DELTA = 0.000001d;

    @Test
    public void sortArrayTest() {
        String[] array = {"a", "e", "b", "c", "d"};
        String[] exceptedAsc = {"a", "b", "c", "d", "e"};
        String[] exceptedDesc = {"e", "d", "c", "b", "a"};
        Assert.assertArrayEquals(exceptedAsc, LuxoftUtils.sortArray(array, true));
        Assert.assertArrayEquals(exceptedDesc, LuxoftUtils.sortArray(array, false));
    }

    @Test
    public void wordAverageLengthTest() {
        String text = "Window was open yesterday";
        Assert.assertEquals(5.5d, LuxoftUtils.wordAverageLength(text), DELTA);
    }

    @Test
    public void reverseWordsTest() {
        String text = "some text to  process   ";
        Assert.assertEquals("emos txet ot  ssecorp   ", LuxoftUtils.reverseWords(text));
        Assert.assertEquals("tenretni", LuxoftUtils.reverseWords("internet"));
    }

    @Test
    public void getCharEntriesTest() {
        String text = "I have a cat";
        final char[] SORTED_CHARS = {'a', 'I', 'c', 'e', 'h', 't', 'v'};
        Assert.assertArrayEquals(SORTED_CHARS, LuxoftUtils.getCharEntries(text));
        Assert.assertArrayEquals(new char[]{'A', 'S', 'a', 'z'}, LuxoftUtils.getCharEntries("aaaAAA zzzSSS"));
    }

    @Test
    public void calculateOverallAreaTest() {
        List<Figure> figures = new ArrayList<Figure>();
        figures.add(new Circle(2d));
        figures.add(new Square(3d));
        figures.add(new Hexagon(4d));
        final double SUM_OF_SQUARES = 63.1355899d;
        Assert.assertEquals(SUM_OF_SQUARES, LuxoftUtils.calculateOverallArea(figures), DELTA);
    }

}
