package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

public class LuxoftUtilsTest {

    @Test
    public void getMonthNameEnTest() {
        Assert.assertEquals("January", LuxoftUtils.getMonthName(1, "en"));
        Assert.assertEquals("February", LuxoftUtils.getMonthName(2, "en"));
        Assert.assertEquals("March", LuxoftUtils.getMonthName(3, "en"));
        Assert.assertEquals("April", LuxoftUtils.getMonthName(4, "en"));
        Assert.assertEquals("May", LuxoftUtils.getMonthName(5, "en"));
        Assert.assertEquals("June", LuxoftUtils.getMonthName(6, "en"));
        Assert.assertEquals("July", LuxoftUtils.getMonthName(7, "en"));
        Assert.assertEquals("August", LuxoftUtils.getMonthName(8, "en"));
        Assert.assertEquals("September", LuxoftUtils.getMonthName(9, "en"));
        Assert.assertEquals("October", LuxoftUtils.getMonthName(10, "en"));
        Assert.assertEquals("November", LuxoftUtils.getMonthName(11, "en"));
        Assert.assertEquals("December", LuxoftUtils.getMonthName(12, "en"));
    }

    @Test
    public void getMonthNameRuTest() {
        Assert.assertEquals("Январь", LuxoftUtils.getMonthName(1, "ru"));
        Assert.assertEquals("Февраль", LuxoftUtils.getMonthName(2, "ru"));
        Assert.assertEquals("Март", LuxoftUtils.getMonthName(3, "ru"));
        Assert.assertEquals("Апрель", LuxoftUtils.getMonthName(4, "ru"));
        Assert.assertEquals("Май", LuxoftUtils.getMonthName(5, "ru"));
        Assert.assertEquals("Июнь", LuxoftUtils.getMonthName(6, "ru"));
        Assert.assertEquals("Июль", LuxoftUtils.getMonthName(7, "ru"));
        Assert.assertEquals("Август", LuxoftUtils.getMonthName(8, "ru"));
        Assert.assertEquals("Сентябрь", LuxoftUtils.getMonthName(9, "ru"));
        Assert.assertEquals("Октябрь", LuxoftUtils.getMonthName(10, "ru"));
        Assert.assertEquals("Ноябрь", LuxoftUtils.getMonthName(11, "ru"));
        Assert.assertEquals("Декабрь", LuxoftUtils.getMonthName(12, "ru"));
    }

    @Test
    public void getMonthNameErrorTest() {
        String ruUnknownMonth = "Неизвестный месяц";
        String enUnknownMonth = "Unknown Month";
        String unknownLanguage = "Unknown Language";
        Assert.assertEquals(ruUnknownMonth, LuxoftUtils.getMonthName(-20, "ru"));
        Assert.assertEquals(ruUnknownMonth, LuxoftUtils.getMonthName(20, "ru"));
        Assert.assertEquals(enUnknownMonth, LuxoftUtils.getMonthName(-20, "en"));
        Assert.assertEquals(enUnknownMonth, LuxoftUtils.getMonthName(20, "en"));
        Assert.assertEquals(unknownLanguage, LuxoftUtils.getMonthName(-20, "hindi"));
        Assert.assertEquals(unknownLanguage, LuxoftUtils.getMonthName(2, "hindi"));
    }

    @Test
    public void binaryToDecimalTest() {
        Assert.assertEquals("15", LuxoftUtils.binaryToDecimal("1111"));
        Assert.assertEquals("3002766244271445",
                LuxoftUtils.binaryToDecimal("1010101010101111111111111111010101010101010101010101"));
        Assert.assertEquals("15", LuxoftUtils.binaryToDecimal("0001111"));
    }

    @Test
    public void binaryToDecimalErrorTest() {
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("23"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(null));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(""));
    }

    @Test
    public void decimalToBinaryTest() {
        Assert.assertEquals("11111111", LuxoftUtils.decimalToBinary("255"));
        Assert.assertEquals("11111111", LuxoftUtils.decimalToBinary("00255"));
        Assert.assertEquals("1010101010101111111111111111010101010101010101010101",
                LuxoftUtils.decimalToBinary("3002766244271445"));
    }

    @Test
    public void decimalToBinaryErrorTest() {
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("ffa23"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(""));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(null));
    }

    @Test
    public void sortArrayTest() {
        int[] array = {2, 4, 1, 5, 6, 3, 8};
        Assert.assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 8}, LuxoftUtils.sortArray(array, true));
        Assert.assertArrayEquals(new int[] {8, 6, 5, 4, 3, 2, 1}, LuxoftUtils.sortArray(array, false));
    }

    @Test
    public void sortArrayErrorTest() {
        Assert.assertNull(LuxoftUtils.sortArray(null, true));
        Assert.assertNull(LuxoftUtils.sortArray(null, false));
    }

}
