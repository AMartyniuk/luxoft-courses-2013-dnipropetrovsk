package com.luxoft.dnepr.courses.regular.unit17.storage;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class EntityStorage {

    private final static String DELIMITER = ",";
    private final static String EQUAL = "=";

    private final static String INSERT_QUERY = "INSERT INTO %s (%s) VALUES (%s)";
    private final static String UPDATE_QUERY = "UPDATE %s SET %s WHERE id = %s";
    private final static String GET_BY_ID_QUERY = "SELECT * FROM %s WHERE id = %s";
    private final static String DELETE_QUERY = "DELETE FROM %s WHERE id = %s";

    private static final String ID_FIELD = "id";

    private DataBase dataBase;

    public EntityStorage() {
        dataBase = new DataBase();
        dataBase.connect();
        dataBase.createSchemes();
    }

    public DataBase getDataBase() {
        return dataBase;
    }

    public Object save(Object object) {
        String tableName = ReflectionUtils.getEntityName(object.getClass());
        Field[] fields = ReflectionUtils.getFields(object);
        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();
        for (Field field : fields) {
            String fieldName = field.getName();
            columns.append(fieldName).append(DELIMITER);
            field.setAccessible(true);
            try {
                Object value = field.get(object);
                values.append(value).append(DELIMITER);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        columns.deleteCharAt(columns.length() - 1);
        values.deleteCharAt(values.length() - 1);
        String query = String.format(INSERT_QUERY, tableName, columns, values);
        if (dataBase.executeStatement(query)) {
            return object;
        }
        return null;
    }

    public Object update(Object object) {
        String tableName = ReflectionUtils.getEntityName(object.getClass());
        Field[] fields = ReflectionUtils.getFields(object);
        StringBuilder values = new StringBuilder();
        String id = null;
        for (Field field : fields) {
            String fieldName = field.getName();
            values.append(fieldName).append(EQUAL);
            field.setAccessible(true);
            try {
                Object value = field.get(object);
                if (fieldName.equalsIgnoreCase(ID_FIELD)) {
                    id = value.toString();
                }
                values.append(value).append(DELIMITER);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        values.deleteCharAt(values.length() - 1);
        String query = String.format(UPDATE_QUERY, tableName, values, id);
        if (dataBase.executeStatement(query)) {
            return object;
        }
        return null;
    }

    public Object get(Class cl, long id) {
        Object resultEntity = null;
        String tableName = ReflectionUtils.getEntityName(cl);
        String query = String.format(GET_BY_ID_QUERY, tableName, id);
        try {
            resultEntity = cl.newInstance();
            try (Statement statement = dataBase.getConnection().createStatement()) {
                ResultSet resultSet = statement.executeQuery(query);
                if (!fillObjectData(resultSet, resultEntity)) {
                    resultEntity = null;
                }
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return resultEntity;
    }

    public boolean delete(Object object) {
        String tableName = ReflectionUtils.getEntityName(object.getClass());
        Field[] fields = ReflectionUtils.getFields(object);
        String id = null;
        for (Field field : fields) {
            if (field.getName().equalsIgnoreCase(ID_FIELD)) {
                field.setAccessible(true);
                try {
                    id = field.get(object).toString();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        String query = String.format(DELETE_QUERY, tableName, id);
        return dataBase.executeStatement(query);
    }

    private boolean fillObjectData(ResultSet resultSet, Object resultEntity) throws SQLException {
        Field[] fields = ReflectionUtils.getFields(resultEntity);
        ResultSetMetaData metaData = resultSet.getMetaData();
        boolean hasData = false;
        while (resultSet.next()) {
            hasData = true;
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                String columnName = metaData.getColumnName(i);
                Object value = resultSet.getObject(i);
                for (Field field : fields) {
                    if (field.getName().equalsIgnoreCase(columnName)) {
                        field.setAccessible(true);
                        try {
                            field.set(resultEntity, value);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return hasData;
    }

}
