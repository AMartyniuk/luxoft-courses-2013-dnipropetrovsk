package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Employee;

public class EmployeeDaoImpl extends AbstractDaoImpl<Employee> {

    @Override
    public Employee save(Employee employee) {
        return super.save(employee);
    }

    @Override
    public Employee update(Employee employee) {
        return super.update(employee);
    }

    @Override
    public Employee get(long id) {
        return super.get(id);
    }

    @Override
    public boolean delete(long id) {
        return super.delete(id);
    }

}
