package com.luxoft.dnepr.courses.regular.unit17.dao;

import com.luxoft.dnepr.courses.regular.unit17.model.Employee;
import com.luxoft.dnepr.courses.regular.unit17.storage.EntityStorage;

public class EmployeeDaoImpl extends AbstractDaoImpl<Employee> {

    public EmployeeDaoImpl(EntityStorage storage) {
        super(storage);
        clazz = Employee.class;
    }
}
