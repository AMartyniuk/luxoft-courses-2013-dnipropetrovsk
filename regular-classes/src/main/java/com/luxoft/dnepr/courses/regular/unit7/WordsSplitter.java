package com.luxoft.dnepr.courses.regular.unit7;

import java.util.ArrayList;
import java.util.List;

public class WordsSplitter {

    public String[] split(String line) {
        List<String> words = new ArrayList<>();
        StringBuilder word = new StringBuilder();
        for (int i = 0; i < line.length(); i++) {
            char symbol = line.charAt(i);
            if (isWordSymbol(symbol)) {
                word.append(symbol);
            } else {
                if (word.length() > 0) {
                    words.add(word.toString());
                    word.delete(0, word.length());
                }
            }
        }
        if (word.length() > 0) {
            words.add(word.toString());
            word.delete(0, word.length());
        }
        return words.toArray(new String[words.size()]);
    }

    private boolean isWordSymbol(char symbol) {
        return Character.isLetter(symbol) || Character.isDigit(symbol);
    }


}
