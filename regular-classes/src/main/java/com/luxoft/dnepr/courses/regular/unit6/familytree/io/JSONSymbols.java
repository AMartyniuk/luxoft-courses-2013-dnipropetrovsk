package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

public interface JSONSymbols {

    static final char OPEN_BRACKET = '{';
    static final char CLOSE_BRACKET = '}';
    static final char QUOTE_SYMBOL = '"';
    static final char KEYVALUE_DELIMITER = ':';
    static final char HASH_ITEMS_DELIMITER = ',';

}
