package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;

import static com.luxoft.dnepr.courses.regular.unit6.familytree.io.JSONSymbols.*;

public class JSONFamilyTree {

    private static final String ROOT_ATTRIBUTE = "root";

    public static String getJSONString(FamilyTree familyTree) {
        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append(OPEN_BRACKET);
        Person root = familyTree.getRoot();
        if (root != null) {
            jsonBuilder.append(QUOTE_SYMBOL + ROOT_ATTRIBUTE + QUOTE_SYMBOL);
            jsonBuilder.append(KEYVALUE_DELIMITER);
            jsonBuilder.append(JSONPerson.getJSONString(root));
        }
        jsonBuilder.append(CLOSE_BRACKET);
        return jsonBuilder.toString();
    }

    public static FamilyTree parseJSON(String JSON) {
        FamilyTree familyTree = null;
        String hash = JSONUtils.getHashWithoutBrackets(JSON);
        String[] keyValues = JSONUtils.getKeyValuePairs(hash);
        if (keyValues.length == 0) {
            return null;
        }
        String keyValue = keyValues[0];
        String key = JSONUtils.getKey(keyValue);
        String value = JSONUtils.getValue(keyValue);
        if (key.equals(ROOT_ATTRIBUTE) && JSONUtils.isHashItem(value)) {
            Person person = JSONPerson.parseJSON(value);
            familyTree = FamilyTreeImpl.create(person);
        }
        return familyTree;
    }

}
