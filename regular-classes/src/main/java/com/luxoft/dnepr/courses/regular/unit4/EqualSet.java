package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

public class EqualSet<E> implements Set<E> {

    private List<E> elements = new ArrayList<>();

    public EqualSet() {

    }

    public EqualSet(Collection<? extends E> collection) {
        addAll(collection);
    }

    @Override
    public boolean add(E e) {
        if (!elements.contains(e)) {
            elements.add(e);
            return true;
        }
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        boolean modified = false;
        for (E element : collection) {
            if (add(element)) {
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public void clear() {
        elements.clear();
    }

    @Override
    public boolean contains(Object o) {
        return elements.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        for (Object e : collection)
            if (!contains(e))
                return false;
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Set)) return false;
        Collection collection = (Collection) obj;
        if (collection.size() != size()) return false;
        try {
            return containsAll(collection);
        } catch (ClassCastException unused) {
            return false;
        } catch (NullPointerException unused) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            E obj = it.next();
            if (obj != null) {
                hash += obj.hashCode();
            }
        }
        return hash;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new EqualSetIterator<E>();
    }

    @Override
    public boolean remove(Object o) {
        return elements.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return elements.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        boolean modified = false;
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public Object[] toArray() {
        return elements.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return elements.toArray(a);
    }

    @Override
    public String toString() {
        return elements.toString();
    }

    private class EqualSetIterator<E> implements Iterator<E> {

        private int cursor = -1;
        private int lastReturn = -1;

        @Override
        public boolean hasNext() {
            return cursor < elements.size() - 1;
        }

        @Override
        public E next() {
            if (cursor + 1 >= elements.size()) {
                throw new NoSuchElementException();
            }
            cursor = cursor + 1;
            return (E) elements.get(lastReturn = cursor);
        }

        @Override
        public void remove() {
            if (lastReturn == -1) {
                throw new IllegalStateException();
            }
            elements.remove(lastReturn);
            cursor--;
            lastReturn = -1;
        }
    }

}
