package com.luxoft.dnepr.courses.regular.unit14.controller;

import com.luxoft.dnepr.courses.regular.unit14.model.Words;
import com.luxoft.dnepr.courses.regular.unit14.model.Vocabulary;
import com.luxoft.dnepr.courses.regular.unit14.view.ConsoleInterface;

import java.io.IOException;

public class MainController {

    private static final String ERROR_MESSAGE = "Error during read file '%s'!";
    private static final String EXIT_KEY = "exit";
    private static final String HELP_KEY = "help";
    private static final String YES_KEY = "y";

    private Vocabulary vocabulary;
    private ConsoleInterface anInterface;

    public MainController() {
        initialize();
    }

    private void initialize() {
        try {
            vocabulary = new Vocabulary();
        } catch (IOException e) {
            System.out.println(String.format(ERROR_MESSAGE, Vocabulary.SONNETS_FILE));
        }
        anInterface = new ConsoleInterface(System.in, System.out);
    }

    public void start() {
        boolean run = true;
        while (run) {
            String word = vocabulary.getWordAtRandom();
            anInterface.printQuestion(word);
            String statement = anInterface.readAnswer();
            if (statement.equalsIgnoreCase(EXIT_KEY)) {
                double result = Words.result(vocabulary.size());
                anInterface.printResult(result);
                run = false;
            } else if (statement.equals(HELP_KEY)) {
                anInterface.printHelp();
            } else {
                boolean answer = statement.equalsIgnoreCase(YES_KEY);
                Words.answer(word, answer);
            }
        }
    }

}
