package com.luxoft.dnepr.courses.unit2.model;

public class Square extends Figure {

    public Square(double side) {
        this.field = side;
    }

    @Override
    public double calculateArea() {
        return field * field;
    }
}
