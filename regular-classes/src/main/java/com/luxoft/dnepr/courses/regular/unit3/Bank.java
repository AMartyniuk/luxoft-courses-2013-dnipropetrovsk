package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Map;

public class Bank implements BankInterface {

    private final String JAVA_VERSION_KEY = "java.version";
    private final String ILLEGAL_JAVA_VERSION_MESSAGE = "Illegal Java version! Actual is %s, but excepted is %s!";
    private final String USER_NOT_FOUND_MESSAGE = "User '%s' not found!";
    private final String WALLET_IS_BLOCKED_MESSAGE = "User '%s' wallet is blocked";
    private final String NOT_ENOUGH_MONEY_MESSAGE = "User '%s' has insufficient funds (%s < %s)";
    private final String WALLET_LIMIT_EXCEEDED_MESSAGE = "User '%s' wallet limit exceeded (%s + %s > %s)";

    private Map<Long, UserInterface> users;

    private final String DECIMAL_OUT_PATTERN = "0.00";
    private final char DECIMAL_SEPARATOR = '.';
    private final DecimalFormat DECIMAL_FORMAT;

    {
        DECIMAL_FORMAT = new DecimalFormat(DECIMAL_OUT_PATTERN);
        DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
        decimalFormatSymbols.setDecimalSeparator(DECIMAL_SEPARATOR);
        DECIMAL_FORMAT.setDecimalFormatSymbols(decimalFormatSymbols);
    }

    /**
     * @param expectedJavaVersion represent current JVM version
     * @throws IllegalJavaVersionError if current JVM version not equal to excepted
     */
    public Bank(String expectedJavaVersion) {
        String actualJavaVersion = System.getProperty(JAVA_VERSION_KEY);
        if (!actualJavaVersion.equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion,
                    String.format(ILLEGAL_JAVA_VERSION_MESSAGE, actualJavaVersion, expectedJavaVersion)
            );
        }
    }

    /**
     * Make money transfer from one user to another
     *
     * @param fromUserId id of sender user
     * @param toUserId   id of recipient user
     * @param amount     money to transfer
     * @throws NoUserFoundException if one of users not found
     * @throws TransactionException if sender has not enough money
     *                              or recipient has smaller limit of wallet amount that sum after
     *                              transfer
     */
    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        UserInterface userFrom = users.get(fromUserId);
        if (userFrom == null) {
            throw new NoUserFoundException(fromUserId, String.format(USER_NOT_FOUND_MESSAGE, userFrom));
        }
        UserInterface userTo = users.get(toUserId);
        if (userTo == null) {
            throw new NoUserFoundException(fromUserId, String.format(USER_NOT_FOUND_MESSAGE, userTo));
        }
        WalletInterface walletFrom = getAndCheckUserFromWallet(userFrom, amount);
        WalletInterface walletTo = getAndCheckUserToWallet(userTo, amount);
        walletFrom.withdraw(amount);
        walletTo.transfer(amount);
    }

    private WalletInterface getAndCheckUserFromWallet(UserInterface userFrom, BigDecimal amount) throws TransactionException {
        WalletInterface walletFrom = userFrom.getWallet();
        try {
            walletFrom.checkWithdrawal(amount);
        } catch (WalletIsBlockedException e) {
            String exceptionMessage = String.format(WALLET_IS_BLOCKED_MESSAGE, userFrom.getName());
            TransactionException transactionException = new TransactionException(exceptionMessage);
            transactionException.addSuppressed(e);
            throw transactionException;
        } catch (InsufficientWalletAmountException e) {
            String exceptionMessage = String.format(
                    NOT_ENOUGH_MONEY_MESSAGE,
                    userFrom.getName(),
                    DECIMAL_FORMAT.format(e.getAmountInWallet()),
                    DECIMAL_FORMAT.format(e.getAmountToWithdraw())
            );
            TransactionException transactionException = new TransactionException(exceptionMessage);
            transactionException.addSuppressed(e);
            throw transactionException;
        }
        return walletFrom;
    }

    private WalletInterface getAndCheckUserToWallet(UserInterface userTo, BigDecimal amount) throws TransactionException {
        WalletInterface walletTo = userTo.getWallet();
        try {
            walletTo.checkTransfer(amount);
        } catch (WalletIsBlockedException e) {
            String exceptionMessage = String.format(WALLET_IS_BLOCKED_MESSAGE, userTo.getName());
            TransactionException transactionException = new TransactionException(exceptionMessage);
            transactionException.addSuppressed(e);
            throw transactionException;
        } catch (LimitExceededException e) {
            String exceptionMessage = String.format(
                    WALLET_LIMIT_EXCEEDED_MESSAGE,
                    userTo.getName(),
                    DECIMAL_FORMAT.format(e.getAmountInWallet()),
                    DECIMAL_FORMAT.format(e.getAmountToTransfer()),
                    DECIMAL_FORMAT.format(walletTo.getMaxAmount())
            );
            TransactionException transactionException = new TransactionException(exceptionMessage);
            transactionException.addSuppressed(e);
            throw transactionException;
        }
        return walletTo;
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }
}
