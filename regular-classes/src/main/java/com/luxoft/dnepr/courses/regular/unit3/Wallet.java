package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {

    private final String WALLET_IS_BLOCKED_MESSAGE = "Wallet is blocked!";
    private final String INSUFFICIENT_WALLET_AMOUNT_MESSAGE = "Not enough money in wallet!";
    private final String LIMIT_EXCEEDED_MESSAGE = "Wallet limit exceeded!";

    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Wallet() {

    }

    public Wallet(Long id, BigDecimal amount, BigDecimal maxAmount) {
        this.id = id;
        this.amount = amount;
        this.maxAmount = maxAmount;
        status = WalletStatus.ACTIVE;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id, WALLET_IS_BLOCKED_MESSAGE);
        }
        if (amountToWithdraw.compareTo(amount) > 0) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount,
                    INSUFFICIENT_WALLET_AMOUNT_MESSAGE
            );
        }
    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        amount = amount.subtract(amountToWithdraw);
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id, WALLET_IS_BLOCKED_MESSAGE);
        }
        if (amount.add(amountToTransfer).compareTo(maxAmount) > 1) {
            throw new LimitExceededException(id, amountToTransfer, amount, LIMIT_EXCEEDED_MESSAGE);
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        amount = amount.add(amountToTransfer);
    }

}
