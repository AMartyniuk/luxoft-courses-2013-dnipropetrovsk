package com.luxoft.dnepr.courses.regular.unit17.storage;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ReflectionUtils {

    private final static String DELIMITER = ".";

    private ReflectionUtils() {}

    private static Field[] findFields(Class cl) {
        List<Field> fieldList = new ArrayList<>();
        if (cl.getSuperclass() != null) {
            Field[] fields = findFields(cl.getSuperclass());
            fieldList.addAll(Arrays.asList(fields));
        }
        fieldList.addAll(Arrays.asList(cl.getDeclaredFields()));
        return fieldList.toArray(new Field[fieldList.size()]);
    }

    public static Field[] getFields(Object object) {
        return findFields(object.getClass());
    }

    public static String getEntityName(Class cl) {
        String entityName = cl.getName();
        int pos = entityName.lastIndexOf(DELIMITER) + DELIMITER.length();
        return entityName.substring(pos);
    }

}
