package com.luxoft.dnepr.courses.unit1;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

public final class LuxoftUtils {

    private static final String ruUnknownMonth = "Неизвестный месяц";
    private static final String[] ruMonths = {
            "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
    };

    private static final String enUnknownMonth = "Unknown Month";
    private static final String[] enMonths = {
            "January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
    };

    private LuxoftUtils() {

    }

    public static String getMonthName(int monthOrder, String language) {
        String[] languageMonths;
        String unknownMonth;
        if (language.equalsIgnoreCase("en")) {
            languageMonths = enMonths;
            unknownMonth = enUnknownMonth;
        } else if (language.equalsIgnoreCase("ru")) {
            languageMonths = ruMonths;
            unknownMonth = ruUnknownMonth;
        } else return "Unknown Language";
        if (monthOrder >= 1 && monthOrder <= 12) {
            return languageMonths[monthOrder - 1];
        } else {
            return unknownMonth;
        }
    }

    public static String binaryToDecimal(String binaryNumber) {
        if (binaryNumber == null || binaryNumber.isEmpty() || !isBinary(binaryNumber)) {
            return "Not binary";
        }
        binaryNumber = clearLeftZeros(binaryNumber);
        BigInteger number = BigInteger.ZERO;
        for (int i = 0; i < binaryNumber.length(); i++) {
            if (binaryNumber.charAt(i) == '1') {
                BigInteger powerOfTwo = BigInteger.valueOf(2);
                int exp = binaryNumber.length() - i - 1;
                powerOfTwo = powerOfTwo.pow(exp);
                number = number.add(powerOfTwo);
            }
        }
        return number.toString();
    }

    private static String clearLeftZeros(String digitString) {
        int indexOfNonZero = 0;
        for (int i = 0; i < digitString.length(); i++) {
            if (digitString.charAt(i) != '0') {
                indexOfNonZero = i;
                break;
            }
        }
        return digitString.substring(indexOfNonZero);
    }

    private static boolean isBinary(String binaryNumber) {
        for (int i = 0; i < binaryNumber.length(); i++) {
            if (!(binaryNumber.charAt(i) == '0' || binaryNumber.charAt(i) == '1')) {
                return false;
            }
        }
        return true;
    }

    public static String decimalToBinary(String decimalNumber) {
        if (decimalNumber == null || decimalNumber.isEmpty() || !isDecimal(decimalNumber)) {
            return "Not decimal";
        }
        decimalNumber = clearLeftZeros(decimalNumber);
        BigInteger digit = new BigInteger(decimalNumber);
        StringBuilder result = new StringBuilder();
        BigInteger two = BigInteger.valueOf(2);
        while (digit.compareTo(BigInteger.ZERO) > 0) {
            BigInteger mod = digit.mod(two);
            digit = digit.divide(two);
            result.append(mod);
        }
        return result.reverse().toString();
    }

    private static boolean isDecimal(String decimalNumber) {
        List<Character> decimalSymbols = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        for (int i = 0; i < decimalNumber.length(); i++) {
            if (!decimalSymbols.contains(decimalNumber.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static int[] sortArray(int[] array, boolean asc) {
        if (array == null) return array;
        int[] workArray = Arrays.copyOf(array, array.length);
        boolean sort = false;
        while (!sort) {
            sort = true;
            for (int i = 0; i < workArray.length - 1; i++) {
                if (workArray[i] > workArray[i + 1] == asc) {
                    int tmp = workArray[i];
                    workArray[i] = workArray[i + 1];
                    workArray[i + 1] = tmp;
                    sort = false;
                }
            }
        }
        return workArray;
    }

}
