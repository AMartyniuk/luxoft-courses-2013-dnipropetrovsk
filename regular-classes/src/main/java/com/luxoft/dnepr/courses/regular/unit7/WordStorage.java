package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Represents word statistics storage.
 */
public class WordStorage {

    private ConcurrentMap<String, Integer> map = new ConcurrentHashMap<>();

    /**
     * Saves given word and increments count of occurrences.
     * @param word
     */
    public void save(String word) {
        map.putIfAbsent(word, 0);
        boolean update = false;
        while (!update) {
            Integer amount = map.get(word);
            update = map.replace(word, amount, amount + 1);
        }
    }

    /**
     * @return unmodifiable map containing voc and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {
        Map<String, Integer> resultMap = Collections.unmodifiableMap(new HashMap<>(map));
        return resultMap;
    }
}
