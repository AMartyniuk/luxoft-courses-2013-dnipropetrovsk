package com.luxoft.dnepr.courses.regular.unit7.scanners;

import java.io.File;
import java.util.concurrent.BlockingQueue;

public class ScannerUtils {

    public final static File FINISH_MARKER = new File("");

    public static void putFinishMarker(BlockingQueue<File> queue) {
        boolean finish = false;
        while (!finish) {
            try {
                queue.put(FINISH_MARKER);
            } catch (InterruptedException e) {
                continue;
            }
            finish = true;
        }
    }

}
