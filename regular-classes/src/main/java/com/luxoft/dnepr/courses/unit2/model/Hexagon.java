package com.luxoft.dnepr.courses.unit2.model;

public class Hexagon extends Figure {

    public Hexagon(double side) {
        this.field = side;
    }

    @Override
    public double calculateArea() {
        return (3 * Math.sqrt(3) * field * field) / 2d;
    }
}
