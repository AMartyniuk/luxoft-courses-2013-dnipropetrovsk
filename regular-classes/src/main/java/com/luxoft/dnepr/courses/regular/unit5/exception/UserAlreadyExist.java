package com.luxoft.dnepr.courses.regular.unit5.exception;


public class UserAlreadyExist extends RuntimeException {

    private final static String USER_EXIST_MESSAGE = "User with id = '%s' already exist!";

    private Long id;

    public UserAlreadyExist(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getMessage() {
        return String.format(USER_EXIST_MESSAGE, id);
    }
}
