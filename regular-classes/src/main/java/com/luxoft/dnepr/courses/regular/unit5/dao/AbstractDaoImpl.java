package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorageUtils;

import java.util.Map;


public class AbstractDaoImpl<E extends Entity> implements IDao<E> {

    @Override
    public E save(E e) {
        if (e == null) {
            throw new IllegalArgumentException("Can't save null reference!");
        }
        Long id = e.getId();
        if (id == null) {
            id = EntityStorageUtils.getNextToMaxId();
            e.setId(id);
        }
        Map<Long, Entity> storage = EntityStorage.getEntities();
        if (storage.containsKey(id)) {
            throw new UserAlreadyExist(id);
        }
        storage.put(id, e);
        return e;
    }

    @Override
    public E update(E e) {
        if (e == null) {
            throw new IllegalArgumentException("Argument 'e' can't be a null reference!");
        }
        Long id = e.getId();
        if (id == null) {
            throw new UserNotFound(id);
        }
        Map<Long, Entity> storage = EntityStorage.getEntities();
        if (!storage.containsKey(id)) {
            throw new UserNotFound(id);
        }
        storage.put(id, e);
        return e;
    }

    @Override
    public E get(long id) {
        Map<Long, Entity> storage = EntityStorage.getEntities();
        return (E) storage.get(id);
    }

    @Override
    public boolean delete(long id) {
        Map<Long, Entity> storage = EntityStorage.getEntities();
        Entity entity = storage.get(id);
        if (entity == null) {
            return false;
        }
        return storage.remove(id) != null;
    }
}
