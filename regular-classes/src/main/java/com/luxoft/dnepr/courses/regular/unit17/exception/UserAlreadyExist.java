package com.luxoft.dnepr.courses.regular.unit17.exception;


public class UserAlreadyExist extends RuntimeException {

    private final static String USER_EXIST_MESSAGE = "User with id = '%s' already exist!";

    private Integer id;

    public UserAlreadyExist(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String getMessage() {
        return String.format(USER_EXIST_MESSAGE, id);
    }
}
