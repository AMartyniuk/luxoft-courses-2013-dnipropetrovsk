package com.luxoft.dnepr.courses.regular.unit7.filters;

import java.io.File;
import java.io.FileFilter;

public class TxtFileFilter implements FileFilter {

    @Override
    public boolean accept(File f) {
        String extension = ".txt";
        return f != null && f.getName().endsWith(extension);
    }

}
