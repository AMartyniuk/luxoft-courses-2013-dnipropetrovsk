package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import java.util.ArrayList;
import java.util.List;

import static com.luxoft.dnepr.courses.regular.unit6.familytree.io.JSONSymbols.*;

public class JSONUtils {

    private JSONUtils() {
    }

    public static String getHashWithoutBrackets(String JSON) {
        int openBracketPos = JSON.indexOf(OPEN_BRACKET) + 1;
        int cloeBracketPos = JSON.lastIndexOf(CLOSE_BRACKET);
        return JSON.substring(openBracketPos, cloeBracketPos);
    }

    public static boolean isHashItem(String item) {
        return item.charAt(0) == OPEN_BRACKET &&
                item.charAt(item.length() - 1) == CLOSE_BRACKET;
    }

    public static String[] getKeyValuePairs(String hash) {
        int inHashDepth = 0;
        int lastIndex = 0;
        List<String> keyValues = new ArrayList<>();
        for (int i = 0; i < hash.length(); i++) {
            char symbol = hash.charAt(i);
            if (symbol == OPEN_BRACKET) inHashDepth++;
            if (symbol == CLOSE_BRACKET) inHashDepth--;
            if (symbol == HASH_ITEMS_DELIMITER && inHashDepth == 0) {
                String keyValueItem = hash.substring(lastIndex, i);
                keyValues.add(keyValueItem);
                lastIndex = i + 1;
            }
        }
        if (lastIndex < hash.length()) {
            String keyValueItem = hash.substring(lastIndex, hash.length());
            keyValues.add(keyValueItem);
        }
        return keyValues.toArray(new String[keyValues.size()]);
    }

    private static String deleteQuotes(String item) {
        if (item.charAt(0) == QUOTE_SYMBOL && item.charAt(item.length() - 1) == QUOTE_SYMBOL) {
            return item.substring(1, item.length() - 1);
        }
        return item;
    }

    public static String getKey(String keyValue) {
        int index = keyValue.indexOf(KEYVALUE_DELIMITER);
        return deleteQuotes(keyValue.substring(0, index));
    }

    public static String getValue(String keyValue) {
        int index = keyValue.indexOf(KEYVALUE_DELIMITER);
        return deleteQuotes(keyValue.substring(index + 1, keyValue.length()));
    }

}
