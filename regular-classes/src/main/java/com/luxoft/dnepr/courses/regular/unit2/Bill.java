package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    private Map<Product, CompositeProduct> compositeProductMap = new HashMap<>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        if (!compositeProductMap.containsKey(product)) {
            CompositeProduct item = new CompositeProduct();
            compositeProductMap.put(product, item);
        }
        CompositeProduct item = compositeProductMap.get(product);
        item.add(product);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return total cost of all products
     */
    public double summarize() {
        Collection<CompositeProduct> compositeProducts = compositeProductMap.values();
        double totalCost = 0;
        for (CompositeProduct product : compositeProducts) {
            totalCost += product.getPrice();
        }
        return totalCost;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        List<Product> products = new ArrayList<Product>(compositeProductMap.values());
        Collections.sort(products, new DescProductComparator());
        return products;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

    private class DescProductComparator implements Comparator<Product> {

        @Override
        public int compare(Product o1, Product o2) {
            return -Double.compare(o1.getPrice(), o2.getPrice());
        }

    }

}
