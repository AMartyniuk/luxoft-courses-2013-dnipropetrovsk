package com.luxoft.dnepr.courses.unit2.model;

public abstract class Figure {

    protected double field;

    public abstract double calculateArea();

}
