package com.luxoft.dnepr.courses.regular.unit17.exception;


public class UserNotFound extends RuntimeException {

    private final static String ID_NOT_FOUND_MESSAGE = "User with id = '%s' not found!";
    private final static String NULL_ID_MESSAGE = "Id field of user is null!";

    private Integer id;

    public UserNotFound(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String getMessage() {
        if (id != null) {
            return String.format(ID_NOT_FOUND_MESSAGE, id);
        } else return NULL_ID_MESSAGE;
    }
}
