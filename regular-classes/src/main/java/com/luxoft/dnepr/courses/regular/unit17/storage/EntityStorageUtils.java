package com.luxoft.dnepr.courses.regular.unit17.storage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class EntityStorageUtils {

    private final static String MAX_ID_QUERY = "SELECT MAX(id) as max_id FROM %s";

    private EntityStorageUtils() {}

    public static Integer getNextToMaxId(EntityStorage storage, Class cl) {
        DataBase dataBase = storage.getDataBase();
        String tableName = ReflectionUtils.getEntityName(cl);
        String query = String.format(MAX_ID_QUERY, tableName);
        Connection connection = dataBase.getConnection();
        Integer max = getMaxId(connection, query);
        return max + 1;
    }

    private static Integer getMaxId(Connection connection, String query) {
        Integer max = 0;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                max = resultSet.getInt("max_id");
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return max;
    }

}
