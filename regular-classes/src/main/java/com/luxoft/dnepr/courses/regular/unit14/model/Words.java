package com.luxoft.dnepr.courses.regular.unit14.model;

import java.util.ArrayList;
import java.util.List;

public class Words {

    private static List<String> knownWords = new ArrayList<>();
    private static List<String> unknownWords = new ArrayList<>();

    public static double result(int total) {
        return total * (knownWords.size() + 1d) / (knownWords.size() + unknownWords.size() + 1d);
    }

    public static void answer(String word, boolean answer) {
        if (answer) {
            knownWords.add(word);
        } else {
            unknownWords.add(word);
        }
    }
}
