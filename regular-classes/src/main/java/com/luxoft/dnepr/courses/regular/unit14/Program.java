package com.luxoft.dnepr.courses.regular.unit14;

import com.luxoft.dnepr.courses.regular.unit14.controller.MainController;

public class Program {

    public static void main(String[] args) {
        new MainController().start();
    }

}
