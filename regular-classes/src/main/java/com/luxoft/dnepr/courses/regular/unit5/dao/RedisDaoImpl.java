package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Redis;

public class RedisDaoImpl extends AbstractDaoImpl<Redis> {

    @Override
    public Redis save(Redis redis) {
        return super.save(redis);
    }

    @Override
    public Redis update(Redis redis) {
        return super.update(redis);
    }

    @Override
    public Redis get(long id) {
        return super.get(id);
    }

    @Override
    public boolean delete(long id) {
        return super.delete(id);
    }

}
