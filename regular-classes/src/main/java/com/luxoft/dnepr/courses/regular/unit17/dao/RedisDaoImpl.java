package com.luxoft.dnepr.courses.regular.unit17.dao;

import com.luxoft.dnepr.courses.regular.unit17.model.Redis;
import com.luxoft.dnepr.courses.regular.unit17.storage.EntityStorage;

public class RedisDaoImpl extends AbstractDaoImpl<Redis> {

    public RedisDaoImpl(EntityStorage storage) {
        super(storage);
        clazz = Redis.class;
    }
}
