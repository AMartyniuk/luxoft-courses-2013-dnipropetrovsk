package com.luxoft.dnepr.courses.regular.unit17.storage;

import java.sql.*;

public class DataBase {

    private final static String DRIVER_NAME = "org.h2.Driver";
    private final static String CONNECTION_STRING = "jdbc:h2:~/test";
    private final static String USER_NAME = "sa";
    private final static String PASSWORD = "";

    private static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS %s (id INTEGER, %s INTEGER, PRIMARY KEY (id))";
    private final static String EMPLOYEE_TABLE = "Employee";
    private final static String REDIS_TABLE = "Redis";
    private final static String SALARY_COLUMN = "salary";
    private final static String WEIGHT_COLUMN = "weight";

    private Connection connection;

    public void connect() {
        try {
            Class.forName(DRIVER_NAME);
            connection = DriverManager.getConnection(CONNECTION_STRING, USER_NAME, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public boolean executeStatement(String query) {
        boolean result;
        try (Statement statement = connection.createStatement()) {
            result = statement.execute(query);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
        return !result;
    }

    public Connection getConnection() {
        return connection;
    }

    public void createSchemes() {
        try (Statement statement = connection.createStatement()) {
            statement.execute(String.format(CREATE_TABLE, EMPLOYEE_TABLE, SALARY_COLUMN));
            statement.execute(String.format(CREATE_TABLE, REDIS_TABLE, WEIGHT_COLUMN));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
