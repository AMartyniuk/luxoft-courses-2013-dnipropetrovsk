package com.luxoft.dnepr.courses.regular.unit5.storage;

import java.util.Set;

public class EntityStorageUtils {

    private EntityStorageUtils() {

    }

    public static Long getNextToMaxId() {
        Long maxId = 0L;
        Set<Long> ids = EntityStorage.getEntities().keySet();
        for (Long id : ids) {
            if (id.compareTo(maxId) > 0) {
                maxId = id;
            }
        }
        return maxId + 1;
    }

}
