package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.util.Objects;

public class PersonImpl implements Person {

    private String name;
    private String ethnicity;
    private Person father;
    private Person mother;
    private Gender gender;
    private int age;

    public PersonImpl() {

    }

    public PersonImpl(String name, Gender gender, String ethnicity, int age, Person father, Person mother) {
        this.name = name;
        this.ethnicity = ethnicity;
        this.gender = gender;
        this.age = age;
        this.father = father;
        this.mother = mother;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEthnicity() {
        return ethnicity;
    }

    @Override
    public Person getFather() {
        return father;
    }

    @Override
    public Person getMother() {
        return mother;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        PersonImpl person = (PersonImpl) obj;

        return age == person.age &&
                Objects.deepEquals(name, person.name) &&
                Objects.deepEquals(gender, person.gender) &&
                Objects.deepEquals(ethnicity, person.ethnicity) &&
                Objects.deepEquals(father, person.father) &&
                Objects.deepEquals(mother, person.mother);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (ethnicity != null ? ethnicity.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + age;
        return result;
    }
}
