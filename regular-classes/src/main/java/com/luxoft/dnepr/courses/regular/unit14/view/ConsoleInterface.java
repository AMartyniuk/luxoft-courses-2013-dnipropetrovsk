package com.luxoft.dnepr.courses.regular.unit14.view;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class ConsoleInterface {

    private static final String QUESTION_TITLE = "Do you know translation of word '%s'?";

    private Scanner scanner;
    private PrintStream output;

    public ConsoleInterface(InputStream input, PrintStream output) {
        scanner = new Scanner(input);
        this.output = output;
        printIntroduction();
    }

    private void printIntroduction() {
        output.println("Welcome to \"Lingustic analizator\"!");
        output.println("Small instruction how to answer:");
        output.println("Type 'y' - if you know word");
        output.println("Type 'n' - if you don't know word");
        output.println("Type 'help' - to show help");
        output.println("Type 'exit' - for exit");
        output.println();
    }

    public void printQuestion(String word) {
        output.println(String.format(QUESTION_TITLE, word));
        output.print("Your answer: ");
    }

    public String readAnswer() {
        return scanner.next();
    }

    public void printHelp() {
        output.println("Help:");
        output.println("Lingustic analizator v1");
        output.println("Autor: Tushar Brahmacobalol");
        output.println("This program can help you to check your English knowledge level.");
        output.println("Small instruction how to answer:");
        output.println("Type 'y' - if you know word");
        output.println("Type 'n' - if you don't know word");
        output.println("Type 'help' - to show help");
        output.println("Type 'exit' - for exit");
    }

    public void printResult(double result) {
        output.println("Your estimated vocabulary is " + result + " words");
    }

}
