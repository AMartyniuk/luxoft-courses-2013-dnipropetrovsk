package com.luxoft.dnepr.courses.regular.unit7;

import com.luxoft.dnepr.courses.regular.unit7.scanners.DirectoryScanner;
import com.luxoft.dnepr.courses.regular.unit7.scanners.FileScanner;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {

    public static final int MAX_QUEUE_CAPACITY = 100;

    private WordStorage wordStorage = new WordStorage();
    private String rootFolder;
    private int maxNumberOfThreads;
    private BlockingQueue<File> queue = new ArrayBlockingQueue<>(MAX_QUEUE_CAPACITY);

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.maxNumberOfThreads = maxNumberOfThreads;
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
        List<File> files = new ArrayList<>();
        files = Collections.synchronizedList(files);

        File root = new File(rootFolder);
        int producerThreadAmount = maxNumberOfThreads/2;
        ExecutorService executorService = Executors.newFixedThreadPool(producerThreadAmount);
        executorService.execute(new DirectoryScanner(root, executorService, queue));

        int consumerThreadAmount = maxNumberOfThreads/2 + maxNumberOfThreads%2;
        ExecutorService fileParserService = Executors.newFixedThreadPool(consumerThreadAmount);
        List<Future<?>> futures = new ArrayList<>();
        for (int i = 0; i < consumerThreadAmount; i++) {
            Future<?> future = fileParserService.submit(new FileScanner(queue, wordStorage, files));
            futures.add(future);
        }
        for (Future<?> future : futures) {
            try {
                future.get();
            } catch (ExecutionException | InterruptedException e) {
                continue;
            }
        }
        fileParserService.shutdown();
        return new FileCrawlerResults(files, wordStorage.getWordStatistics());
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }

}
