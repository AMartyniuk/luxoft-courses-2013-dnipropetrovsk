package com.luxoft.dnepr.courses.regular.unit17.dao;

import com.luxoft.dnepr.courses.regular.unit17.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit17.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit17.model.Entity;
import com.luxoft.dnepr.courses.regular.unit17.storage.EntityStorage;
import com.luxoft.dnepr.courses.regular.unit17.storage.EntityStorageUtils;

public class AbstractDaoImpl<E extends Entity> implements IDao<E> {

    private EntityStorage storage;
    protected Class clazz;

    public AbstractDaoImpl(EntityStorage storage) {
        this.storage = storage;
    }

    @Override
    public E save(E e) {
        if (e == null) {
            throw new IllegalArgumentException("Can't save null reference!");
        }
        Integer id = e.getId();
        if (id == null) {
            id = EntityStorageUtils.getNextToMaxId(storage, clazz);
            e.setId(id);
        }
        if (get(id) != null) {
            throw new UserAlreadyExist(id);
        }
        return (E) storage.save(e);
    }

    @Override
    public E update(E e) {
        if (e == null) {
            throw new IllegalArgumentException("Argument 'e' can't be a null reference!");
        }
        Integer id = e.getId();
        if (id == null) {
            throw new UserNotFound(id);
        }
        if (get(id) == null) {
            throw new UserNotFound(id);
        }
        return (E) storage.update(e);
    }

    @Override
    public E get(int id) {
        return (E) storage.get(clazz, id);
    }

    @Override
    public boolean delete(E e) {
        if (e == null) {
            throw new IllegalArgumentException("Argument 'e' can't be a null reference!");
        }
        return storage.delete(e);
    }
}
