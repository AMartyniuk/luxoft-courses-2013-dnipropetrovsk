package com.luxoft.dnepr.courses.regular.unit3;


import java.util.Objects;

public class User implements UserInterface {

    private Long id;
    private String name;
    private WalletInterface wallet;

    public User() {

    }

    public User(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public WalletInterface getWallet() {
        return wallet;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        this.wallet = wallet;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        User user = (User) obj;
        return Objects.equals(id, user.id) && Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
