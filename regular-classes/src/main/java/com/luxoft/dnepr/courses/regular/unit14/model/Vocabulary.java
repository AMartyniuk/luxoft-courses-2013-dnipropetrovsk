package com.luxoft.dnepr.courses.regular.unit14.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;

public class Vocabulary {

    public static final String SONNETS_FILE = "sonnets.txt";
    private static final String CRLF = "\r\n";
    public static final int MIN_WORD_LENGTH = 3;

    private Random random = new Random();
    private String[] words;

    public Vocabulary() throws IOException {
        String text = readTextFromFile(SONNETS_FILE);
        words = readWordsFromText(text);
    }

    private String readTextFromFile(String filePath) throws IOException {
        InputStream is = getClass().getResourceAsStream(filePath);
        String text;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String line;
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line).append(CRLF);
            }
            text = stringBuilder.toString();
        }
        return text;
    }

    private String[] readWordsFromText(String text) {
        Set<String> words = new HashSet<>();
        StringTokenizer tokenizer = new StringTokenizer(text);
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (token.length() > MIN_WORD_LENGTH) {
                String word = trimDelimiters(token);
                if (word != null) {
                    words.add(word.toLowerCase());
                }
            }
        }
        return words.toArray(new String[words.size()]);
    }

    private String trimDelimiters(String token) {
        int start = 0;
        while (start < token.length() && !Character.isAlphabetic(token.charAt(start))) {
            start++;
        }
        int end = token.length() - 1;
        while (end >= 0 && !Character.isAlphabetic(token.charAt(end))) {
            end--;
        }
        if (end > start) {
            return token.substring(start, end + 1);
        }
        return null;
    }

    public int size() {
        return words.length;
    }

    public String getWordAtRandom() {
        return words[random.nextInt(words.length)];
    }
}
