package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;

import java.io.*;

public class IOUtils {

    private IOUtils() {
    }

    public static FamilyTree load(String filename) {
        File file = new File(filename);
        FamilyTree familyTree = null;
        if (file.exists() && file.isFile()) {
            try (FileInputStream fis = new FileInputStream(file)) {
                familyTree = load(fis);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return familyTree;
    }

    public static FamilyTree load(InputStream is) {
        FamilyTree familyTree = null;
        try (ObjectInputStream ois = new ObjectInputStream(is)) {
            familyTree = (FamilyTree) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return familyTree;
    }

    public static void save(String filename, FamilyTree familyTree) {
        File file = new File(filename);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            save(fos, familyTree);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void save(OutputStream os, FamilyTree familyTree) {
        try (ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeObject(familyTree);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
