package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;

import static com.luxoft.dnepr.courses.regular.unit6.familytree.io.JSONSymbols.*;

public class JSONPerson {

    private static final String EMPTY_STRING = "";
    //Pattern = "${key}"${keyvalueDelimiter}${value}${hashItemsDelimiter}
    private static final String KEYVALUE_PATTERN = "\"%s\"%s%s%s";

    private static final String NAME_ATTRIBUTE = "name";
    private static final String GENDER_ATTRIBUTE = "gender";
    private static final String ETHNICITY_ATTRIBUTE = "ethnicity";
    private static final String AGE_ATTRIBUTE = "age";
    private static final String FATHER_ATTRIBUTE = "father";
    private static final String MOTHER_ATTRIBUTE = "mother";

    public static String getJSONString(Person person) {
        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append(OPEN_BRACKET);

        jsonBuilder.append(getAttribute(NAME_ATTRIBUTE, person.getName()));
        jsonBuilder.append(getAttribute(GENDER_ATTRIBUTE, person.getGender()));
        jsonBuilder.append(getAttribute(ETHNICITY_ATTRIBUTE, person.getEthnicity()));
        jsonBuilder.append(getAttribute(AGE_ATTRIBUTE, person.getAge()));
        jsonBuilder.append(getAttribute(FATHER_ATTRIBUTE, person.getFather()));
        jsonBuilder.append(getAttribute(MOTHER_ATTRIBUTE, person.getMother()));

        if (jsonBuilder.charAt(jsonBuilder.length() - 1) == HASH_ITEMS_DELIMITER) {
            jsonBuilder.deleteCharAt(jsonBuilder.length() - 1);
        }

        jsonBuilder.append(CLOSE_BRACKET);
        return jsonBuilder.toString();
    }

    private static String getAttribute(String attributeTitle, Object attribute) {
        boolean isHash = false;
        if (attribute != null) {
            if (attribute instanceof Person) {
                isHash = true;
                attribute = JSONPerson.getJSONString((Person) attribute);
            }
            return String.format(KEYVALUE_PATTERN, attributeTitle,
                    KEYVALUE_DELIMITER, addQuotes(attribute, isHash), HASH_ITEMS_DELIMITER);
        }
        return EMPTY_STRING;
    }

    private static String addQuotes(Object item, boolean isHash) {
        if (isHash) {
            return item.toString();
        } else return QUOTE_SYMBOL + item.toString() + QUOTE_SYMBOL;
    }

    public static PersonImpl parseJSON(String JSON) {
        PersonImpl person = null;
        String hash = JSONUtils.getHashWithoutBrackets(JSON);
        String[] keyValues = JSONUtils.getKeyValuePairs(hash);
        if (keyValues.length > 0) {
            person = new PersonImpl();
        }
        for (String keyValue : keyValues) {
            String key = JSONUtils.getKey(keyValue);
            String value = JSONUtils.getValue(keyValue);
            setAttribute(person, key, value);
        }
        return person;
    }

    private static void setAttribute(PersonImpl person, String key, String value) {
        if (key.equals(NAME_ATTRIBUTE)) {
            person.setName(value);
        }
        if (key.equals(GENDER_ATTRIBUTE)) {
            person.setGender(Gender.valueOf(value));
        }
        if (key.equals(ETHNICITY_ATTRIBUTE)) {
            person.setEthnicity(value);
        }
        if (key.equals(AGE_ATTRIBUTE)) {
            person.setAge(Integer.parseInt(value));
        }
        if (key.equals(FATHER_ATTRIBUTE)) {
            Person father = JSONPerson.parseJSON(value);
            person.setFather(father);
        }
        if (key.equals(MOTHER_ATTRIBUTE)) {
            Person mother = JSONPerson.parseJSON(value);
            person.setMother(mother);
        }
    }

}
