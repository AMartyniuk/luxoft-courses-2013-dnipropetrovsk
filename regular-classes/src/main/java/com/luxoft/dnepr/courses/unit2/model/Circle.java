package com.luxoft.dnepr.courses.unit2.model;

public class Circle extends Figure {

    public Circle(double radius) {
        this.field = radius;
    }

    @Override
    public double calculateArea() {
        return Math.PI * field * field;
    }
}
