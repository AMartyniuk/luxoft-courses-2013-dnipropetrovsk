package com.luxoft.dnepr.courses.regular.unit7.scanners;

import com.luxoft.dnepr.courses.regular.unit7.WordStorage;
import com.luxoft.dnepr.courses.regular.unit7.WordsSplitter;

import java.io.*;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class FileScanner implements Runnable {

    private BlockingQueue<File> queue;
    private WordStorage wordStorage;
    private List<File> files;

    public FileScanner(BlockingQueue<File> queue, WordStorage wordStorage, List<File> files) {
        this.queue = queue;
        this.wordStorage = wordStorage;
        this.files = files;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                File file = queue.take();
                if (file == ScannerUtils.FINISH_MARKER) {
                    ScannerUtils.putFinishMarker(queue);
                    return;
                }
                files.add(file);
                parseFile(file);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (IOException e) {
                continue;
            }
        }
    }

    private void parseFile(File file) throws IOException {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                saveWords(line);
            }
        }
    }

    private void saveWords(String line) {
        String[] words = new WordsSplitter().split(line);
        for (String word : words) {
            wordStorage.save(word);
        }
    }
}
