package com.luxoft.dnepr.courses.regular.unit5.model;


public class Redis extends Entity {

    private int weight;

    public Redis() {

    }

    public Redis(Long id, int weight) {
        setId(id);
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;

        Redis redis = (Redis) obj;

        if (weight != redis.weight) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + weight;
        return result;
    }

}
