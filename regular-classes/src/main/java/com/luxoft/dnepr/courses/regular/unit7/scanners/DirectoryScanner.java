package com.luxoft.dnepr.courses.regular.unit7.scanners;

import com.luxoft.dnepr.courses.regular.unit7.filters.DirectoryFileFilter;
import com.luxoft.dnepr.courses.regular.unit7.filters.TxtFileFilter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

public class DirectoryScanner implements Runnable {

    private static AtomicInteger threadCount = new AtomicInteger(0);

    private ExecutorService executorService;
    private BlockingQueue<File> queue;
    private File root;

    public DirectoryScanner(File root, ExecutorService executorService, BlockingQueue<File> queue) {
        this.root = root;
        this.executorService = executorService;
        this.queue = queue;
    }

    @Override
    public void run() {
        List<Future<?>> futures = new ArrayList<>();
        try {
            File[] files = root.listFiles(new TxtFileFilter());
            if (files != null) {
                for (File file : files) {
                    queue.put(file);
                }
            }
            File[] directories = root.listFiles(new DirectoryFileFilter());
            if (directories != null) {
                for (File directory : directories) {
                    DirectoryScanner scanner = new DirectoryScanner(directory, executorService, queue);
                    threadCount.incrementAndGet();
                    Future<?> future = executorService.submit(scanner);
                    futures.add(future);
                }
            }
        } catch (InterruptedException e) {
            for (Future<?> future : futures) {
                if (!future.isDone()) {
                    future.cancel(true);
                }
            }
        }
        if (threadCount.get() == 0) {
            ScannerUtils.putFinishMarker(queue);
            executorService.shutdown();
        }
        threadCount.decrementAndGet();
    }

}
