package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.JSONFamilyTree;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FamilyTreeImpl implements FamilyTree {

    private static final long serialVersionUID = 3057396458981676327L;
    private Person root;
    private transient long creationTime;

    private FamilyTreeImpl(Person root, long creationTime) {
        this.root = root;
        this.creationTime = creationTime;
    }

    public static FamilyTree create(Person root) {
        return new FamilyTreeImpl(root, System.currentTimeMillis());
    }

    @Override
    public Person getRoot() {
        return root;
    }

    @Override
    public long getCreationTime() {
        return creationTime;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        String JSON = JSONFamilyTree.getJSONString(this);
        oos.writeUTF(JSON);
    }

    private void readObject(ObjectInputStream ois) throws IOException {
        String JSON = ois.readUTF();
        FamilyTree familyTree = JSONFamilyTree.parseJSON(JSON);
        this.root = familyTree.getRoot();
    }

}
