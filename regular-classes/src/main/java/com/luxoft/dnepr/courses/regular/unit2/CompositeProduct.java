package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {

    private final int FIRST_ITEM_INDEX = 0;
    private final int DISCOUNT_SMALL_MIN_COUNT = 2;
    private final int DISCOUNT_NORMAL_MIN_COUNT = 3;

    private final double DISCOUNT_SMALL_PERCENT = 0.05;
    private final double DISCOUNT_NORMAL_PERCENT = 0.1;

    private List<Product> childProducts = new ArrayList<>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        Product item = childProducts.get(FIRST_ITEM_INDEX);
        if (item == null) {
            return null;
        }
        return item.getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        Product item = childProducts.get(FIRST_ITEM_INDEX);
        if (item == null) {
            return null;
        }
        return item.getName();
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        double totalPrice = 0;
        for (Product product : childProducts) {
            totalPrice += product.getPrice();
        }
        double discount = 0;
        if (childProducts.size() >= DISCOUNT_SMALL_MIN_COUNT) {
            discount = DISCOUNT_SMALL_PERCENT;
        }
        if (childProducts.size() >= DISCOUNT_NORMAL_MIN_COUNT) {
            discount = DISCOUNT_NORMAL_PERCENT;
        }
        totalPrice = totalPrice - totalPrice * discount;
        return totalPrice;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
