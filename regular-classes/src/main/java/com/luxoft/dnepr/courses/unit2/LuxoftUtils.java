package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

public final class LuxoftUtils {

    private static final char SPACE_CHAR = ' ';

    private LuxoftUtils() {
    }

    private static boolean compareTwoStrings(String first, String second, boolean asc) {
        return first.compareTo(second) <= 0 && asc || first.compareTo(second) > 0 && !asc;
    }

    private static String[] merge(String[] left, String[] right, boolean asc) {
        String[] result = new String[left.length + right.length];
        int resultIndex = 0;
        int leftIndex = 0;
        int rightIndex = 0;
        while (leftIndex < left.length && rightIndex < right.length) {
            if (compareTwoStrings(left[leftIndex], right[rightIndex], asc)) {
                result[resultIndex++] = left[leftIndex++];
            } else {
                result[resultIndex++] = right[rightIndex++];
            }
        }
        while (leftIndex < left.length) {
            result[resultIndex++] = left[leftIndex++];
        }
        while (rightIndex < right.length) {
            result[resultIndex++] = right[rightIndex++];
        }
        return result;
    }

    private static String[] mergeSort(String[] array, boolean asc) {
        if (array.length <= 1) {
            return array;
        }
        int middle = array.length / 2;
        String[] left = Arrays.copyOf(array, middle);
        String[] right = Arrays.copyOfRange(array, middle, array.length);
        left = mergeSort(left, asc);
        right = mergeSort(right, asc);
        return merge(left, right, asc);
    }

    public static String[] sortArray(String[] array, boolean asc) {
        String[] arrayToSort = array.clone();
        return mergeSort(arrayToSort, asc);
    }

    public static double wordAverageLength(String str) {
        final String REGEX = "\\s+";
        String[] words = str.trim().split(REGEX);
        double sum = 0;
        for (String word : words) {
            sum += word.length();
        }
        return sum / words.length;
    }

    public static String reverseWords(String str) {
        StringBuilder result = new StringBuilder();
        StringBuilder word = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != SPACE_CHAR) {
                word.append(str.charAt(i));
            } else {
                if (word.length() > 0) {
                    String reverseWord = word.reverse().toString();
                    result.append(reverseWord);
                    word.delete(0, word.length());
                }
                result.append(str.charAt(i));
            }
        }
        result.append(word.reverse().toString());
        return result.toString();
    }

    private static char[] getSortCharArray(Map<Character, Integer> map) {
        List<Map.Entry<Character, Integer>> list = new ArrayList<Map.Entry<Character, Integer>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Character, Integer>>() {
            public int compare(Map.Entry<Character, Integer> item1, Map.Entry<Character, Integer> item2) {
                //desc order by value
                int result = -item1.getValue().compareTo(item2.getValue());
                if (result == 0) {
                    //asc order by key
                    return item1.getKey().compareTo(item2.getKey());
                }
                return result;
            }
        });
        char[] result = new char[list.size()];
        int index = 0;
        for (Map.Entry<Character, Integer> item : list) {
            result[index++] = item.getKey();
        }
        return result;
    }

    public static char[] getCharEntries(String str) {
        Map<Character, Integer> charMap = new HashMap<Character, Integer>();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == SPACE_CHAR) {
                continue;
            }
            int count = 1;
            if (charMap.containsKey(str.charAt(i))) {
                count += charMap.get(str.charAt(i));
            }
            charMap.put(str.charAt(i), count);
        }
        return getSortCharArray(charMap);
    }

    public static double calculateOverallArea(List<Figure> figures) {
        double sum = 0;
        for (Figure figure : figures) {
            sum += figure.calculateArea();
        }
        return sum;
    }

}
