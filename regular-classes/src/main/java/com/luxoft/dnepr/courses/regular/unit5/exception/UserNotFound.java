package com.luxoft.dnepr.courses.regular.unit5.exception;


public class UserNotFound extends RuntimeException {

    private final static String ID_NOT_FOUND_MESSAGE = "User with id = '%s' not found!";
    private final static String NULL_ID_MESSAGE = "Id field of user is null!";

    private Long id;

    public UserNotFound(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getMessage() {
        if (id != null) {
            return String.format(ID_NOT_FOUND_MESSAGE, id);
        } else return NULL_ID_MESSAGE;
    }
}
